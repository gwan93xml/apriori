-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 27, 2017 at 04:26 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_tsukamoto`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `kodecustomer` varchar(500) NOT NULL,
  `namacustomer` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `kodecustomer`, `namacustomer`) VALUES
(2, 'C01', 'Sujiman'),
(3, 'C02', 'Sudirman'),
(4, 'C03', 'Sukirman'),
(5, 'C04', 'Supriman');

-- --------------------------------------------------------

--
-- Table structure for table `hasil`
--

CREATE TABLE `hasil` (
  `id` int(11) UNSIGNED NOT NULL,
  `kodecustomer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `namacustomer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hasil`
--

INSERT INTO `hasil` (`id`, `kodecustomer`, `namacustomer`, `keterangan`) VALUES
(38, 'C01', 'Sujiman', 'Sangat Layak'),
(39, 'C02', 'Sudirman', 'Tidak Layak');

-- --------------------------------------------------------

--
-- Table structure for table `kriteria`
--

CREATE TABLE `kriteria` (
  `id` int(11) NOT NULL,
  `kodekriteria` varchar(500) NOT NULL,
  `namakriteria` varchar(500) NOT NULL,
  `useselect` varchar(1) NOT NULL,
  `hasil` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kriteria`
--

INSERT INTO `kriteria` (`id`, `kodekriteria`, `namakriteria`, `useselect`, `hasil`) VALUES
(5, 'K1', 'Pekerjaan', 'Y', 'N'),
(6, 'K2', 'Penghasilan', 'N', 'N'),
(7, 'K3', 'Tanggungan', 'N', 'N'),
(8, 'K4', 'Kelayakan', 'Y', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `username`, `password`, `nama`) VALUES
(1, 'admin', 'admin', 'Administrator');

-- --------------------------------------------------------

--
-- Table structure for table `penilaian`
--

CREATE TABLE `penilaian` (
  `id` int(11) NOT NULL,
  `kodecustomer` varchar(500) NOT NULL,
  `kodekriteria` varchar(500) NOT NULL,
  `nilai` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penilaian`
--

INSERT INTO `penilaian` (`id`, `kodecustomer`, `kodekriteria`, `nilai`) VALUES
(10, 'C01', 'K1', '5'),
(11, 'C01', 'K2', '4500000'),
(12, 'C01', 'K3', '3000000'),
(13, 'C02', 'K1', '3'),
(14, 'C02', 'K2', '2000000'),
(15, 'C02', 'K3', '2000000');

-- --------------------------------------------------------

--
-- Table structure for table `rule`
--

CREATE TABLE `rule` (
  `id` int(11) NOT NULL,
  `koderule` varchar(500) NOT NULL,
  `hasil` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rule`
--

INSERT INTO `rule` (`id`, `koderule`, `hasil`) VALUES
(190, 'R1', 'Tidak Layak'),
(191, 'R2', 'Tidak Layak'),
(192, 'R3', 'Tidak Layak'),
(193, 'R4', 'Tidak Layak'),
(194, 'R5', 'Tidak Layak'),
(195, 'R6', 'Tidak Layak'),
(196, 'R7', 'Cukup Layak'),
(197, 'R8', 'Tidak Layak'),
(198, 'R9', 'Tidak Layak'),
(199, 'R10', 'Tidak Layak'),
(200, 'R11', 'Tidak Layak'),
(201, 'R12', 'Tidak Layak'),
(202, 'R13', 'Sangat Layak'),
(203, 'R14', 'Sangat Layak'),
(204, 'R15', 'Tidak Layak'),
(205, 'R16', 'Tidak Layak'),
(206, 'R17', 'Tidak Layak'),
(207, 'R18', 'Sangat Layak'),
(208, 'R19', 'Tidak Layak'),
(209, 'R20', 'Tidak Layak'),
(210, 'R21', 'Tidak Layak'),
(211, 'R22', 'Tidak Layak'),
(212, 'R23', 'Tidak Layak'),
(213, 'R24', 'Tidak Layak'),
(214, 'R25', 'Tidak Layak'),
(215, 'R26', 'Tidak Layak'),
(216, 'R27', 'Sangat Layak');

-- --------------------------------------------------------

--
-- Table structure for table `subkriteria`
--

CREATE TABLE `subkriteria` (
  `id` int(11) NOT NULL,
  `kodekriteria` varchar(500) NOT NULL,
  `subkriteria` varchar(500) NOT NULL,
  `dari` varchar(191) DEFAULT NULL,
  `hingga` varchar(191) DEFAULT NULL,
  `label` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subkriteria`
--

INSERT INTO `subkriteria` (`id`, `kodekriteria`, `subkriteria`, `dari`, `hingga`, `label`) VALUES
(91, 'K1', 'Rendah', '2', '4', 'Buruh'),
(92, 'K1', 'Sedang', '4', '6', 'TNI'),
(93, 'K1', 'Tinggi', '6', '8', 'PNS'),
(103, 'K4', 'Tidak Layak', '2', '4', 'Tidak Layak'),
(104, 'K4', 'Cukup Layak', '4', '6', 'Cukup Layak'),
(105, 'K4', 'Sangat Layak', '6', '8', 'Sangat Layak'),
(106, 'K2', 'Rendah', '2000000', '3999999', ''),
(107, 'K2', 'Sedang', '4000000', '5999999', ''),
(108, 'K2', 'Tinggi', '6000000', '8000000', ''),
(109, 'K3', 'Rendah', '2000000', '3999999', ''),
(110, 'K3', 'Sedang', '4000000', '5999999', ''),
(111, 'K3', 'Tinggi', '6000000', '8000000', '');

-- --------------------------------------------------------

--
-- Table structure for table `subrule`
--

CREATE TABLE `subrule` (
  `id` int(11) NOT NULL,
  `koderule` varchar(500) NOT NULL,
  `kriteria` varchar(500) NOT NULL,
  `subkriteria` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subrule`
--

INSERT INTO `subrule` (`id`, `koderule`, `kriteria`, `subkriteria`) VALUES
(568, 'R1', 'K1', 'Rendah'),
(569, 'R1', 'K2', 'Rendah'),
(570, 'R1', 'K3', 'Rendah'),
(571, 'R2', 'K1', 'Rendah'),
(572, 'R2', 'K2', 'Rendah'),
(573, 'R2', 'K3', 'Sedang'),
(574, 'R3', 'K1', 'Rendah'),
(575, 'R3', 'K2', 'Rendah'),
(576, 'R3', 'K3', 'Tinggi'),
(577, 'R4', 'K1', 'Rendah'),
(578, 'R4', 'K2', 'Sedang'),
(579, 'R4', 'K3', 'Rendah'),
(580, 'R5', 'K1', 'Rendah'),
(581, 'R5', 'K2', 'Sedang'),
(582, 'R5', 'K3', 'Sedang'),
(583, 'R6', 'K1', 'Rendah'),
(584, 'R6', 'K2', 'Sedang'),
(585, 'R6', 'K3', 'Tinggi'),
(586, 'R7', 'K1', 'Rendah'),
(587, 'R7', 'K2', 'Tinggi'),
(588, 'R7', 'K3', 'Rendah'),
(589, 'R8', 'K1', 'Rendah'),
(590, 'R8', 'K2', 'Tinggi'),
(591, 'R8', 'K3', 'Sedang'),
(592, 'R9', 'K1', 'Rendah'),
(593, 'R9', 'K2', 'Tinggi'),
(594, 'R9', 'K3', 'Tinggi'),
(595, 'R10', 'K1', 'Sedang'),
(596, 'R10', 'K2', 'Rendah'),
(597, 'R10', 'K3', 'Rendah'),
(598, 'R11', 'K1', 'Sedang'),
(599, 'R11', 'K2', 'Rendah'),
(600, 'R11', 'K3', 'Sedang'),
(601, 'R12', 'K1', 'Sedang'),
(602, 'R12', 'K2', 'Rendah'),
(603, 'R12', 'K3', 'Tinggi'),
(604, 'R13', 'K1', 'Sedang'),
(605, 'R13', 'K2', 'Sedang'),
(606, 'R13', 'K3', 'Rendah'),
(607, 'R14', 'K1', 'Sedang'),
(608, 'R14', 'K2', 'Sedang'),
(609, 'R14', 'K3', 'Sedang'),
(610, 'R15', 'K1', 'Sedang'),
(611, 'R15', 'K2', 'Sedang'),
(612, 'R15', 'K3', 'Tinggi'),
(613, 'R16', 'K1', 'Sedang'),
(614, 'R16', 'K2', 'Tinggi'),
(615, 'R16', 'K3', 'Rendah'),
(616, 'R17', 'K1', 'Sedang'),
(617, 'R17', 'K2', 'Tinggi'),
(618, 'R17', 'K3', 'Sedang'),
(619, 'R18', 'K1', 'Sedang'),
(620, 'R18', 'K2', 'Tinggi'),
(621, 'R18', 'K3', 'Tinggi'),
(622, 'R19', 'K1', 'Tinggi'),
(623, 'R19', 'K2', 'Rendah'),
(624, 'R19', 'K3', 'Rendah'),
(625, 'R20', 'K1', 'Tinggi'),
(626, 'R20', 'K2', 'Rendah'),
(627, 'R20', 'K3', 'Sedang'),
(628, 'R21', 'K1', 'Tinggi'),
(629, 'R21', 'K2', 'Rendah'),
(630, 'R21', 'K3', 'Tinggi'),
(631, 'R22', 'K1', 'Tinggi'),
(632, 'R22', 'K2', 'Sedang'),
(633, 'R22', 'K3', 'Rendah'),
(634, 'R23', 'K1', 'Tinggi'),
(635, 'R23', 'K2', 'Sedang'),
(636, 'R23', 'K3', 'Sedang'),
(637, 'R24', 'K1', 'Tinggi'),
(638, 'R24', 'K2', 'Sedang'),
(639, 'R24', 'K3', 'Tinggi'),
(640, 'R25', 'K1', 'Tinggi'),
(641, 'R25', 'K2', 'Tinggi'),
(642, 'R25', 'K3', 'Rendah'),
(643, 'R26', 'K1', 'Tinggi'),
(644, 'R26', 'K2', 'Tinggi'),
(645, 'R26', 'K3', 'Sedang'),
(646, 'R27', 'K1', 'Tinggi'),
(647, 'R27', 'K2', 'Tinggi'),
(648, 'R27', 'K3', 'Tinggi');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hasil`
--
ALTER TABLE `hasil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kriteria`
--
ALTER TABLE `kriteria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penilaian`
--
ALTER TABLE `penilaian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rule`
--
ALTER TABLE `rule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subkriteria`
--
ALTER TABLE `subkriteria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subrule`
--
ALTER TABLE `subrule`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `hasil`
--
ALTER TABLE `hasil`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `kriteria`
--
ALTER TABLE `kriteria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `penilaian`
--
ALTER TABLE `penilaian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `rule`
--
ALTER TABLE `rule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=217;
--
-- AUTO_INCREMENT for table `subkriteria`
--
ALTER TABLE `subkriteria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;
--
-- AUTO_INCREMENT for table `subrule`
--
ALTER TABLE `subrule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=649;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
