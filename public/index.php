<?php
session_start();
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \RedBeanPHP\R;
require __DIR__'/vendor/autoload.php';
require 'modules/LoginController/LoginController.php';
require 'modules/HomeController/HomeController.php';
require 'modules/CustomerController/CustomerController.php';
require 'modules/KriteriaController/KriteriaController.php';
require 'modules/RuleController/RuleController.php';
require 'modules/PenilaianController/PenilaianController.php';
require 'modules/PerhitunganController/PerhitunganController.php';
require 'modules/LaporanController/LaporanController.php';
$smarty = new Smarty;
$db = new R;
$db::setup('mysql:host=localhost;
    dbname=db_apriori','root','');
$root = 'http://localhost/apriori';
$admin = 'http://localhost/apriori/admin';
$user = 'http://localhost/apriori/user';
$dir = __DIR__;
$app_tpl = __DIR__ . "/views/layout/main.tpl";
$style = __DIR__ . "/views/layout/block/style.tpl";
$script = __DIR__ . "/views/layout/block/script.tpl";
$views = __DIR__ . "/views/layout";
$smarty->assign('root',$root);
$smarty->assign('admin',$admin);
$smarty->assign('user',$user);
$smarty->assign('app_tpl',$app_tpl);
$smarty->assign('style',$style);
$smarty->assign('script',$script);
$smarty->assign('views',$views);
$configuration = [
'settings' => [
'displayErrorDetails' => true,
],
'debug' => true,
'path' => [
'root' => 'http://localhost/apriori',
'admin' => 'http://localhost/apriori/admin',
'user' => 'http://localhost/apriori/user',
],
'smarty' => $smarty,
'dir' => $dir,
'db' => $db,
];
$c = new \Slim\Container($configuration);
$app = new \Slim\App($c);

$auth = function ($request,$response,$next) {
    $response = $next($request, $response);
    if(!isset($_SESSION['username'])){
        return $response->withRedirect('login');
    }
    else{
        return $response;
    }
};

if(isset($_SESSION['username'])){
    $smarty->assign('nama',$_SESSION['nama']);
    $smarty->assign('jenis','Admin');
}
$app->get('/', \HomeController::class . ':index')->add($auth);
$app->group('/login', function () use ($app) {
    $app->get('', \LoginController::class . ':index');
    $app->post('', \LoginController::class . ':loginAction');
});
$app->get('/logout', \LoginController::class . ':logout')->add($auth);

$app->group('/customer', function () use ($app) {
    $app->get('', \CustomerController::class . ':index');
    $app->get('/tambah', \CustomerController::class . ':tambah');
    $app->get('/edit/{id}', \CustomerController::class . ':edit');
    $app->post('/hapus/{id}', \CustomerController::class . ':hapus');
    $app->post('/simpan', \CustomerController::class . ':simpan');
    $app->post('/update', \CustomerController::class . ':update');
});

$app->group('/kriteria', function () use ($app) {
    $app->get('', \KriteriaController::class . ':index');
    $app->get('/tambah', \KriteriaController::class . ':tambah');
    $app->get('/edit/{id}', \KriteriaController::class . ':edit');
    $app->post('/simpan', \KriteriaController::class . ':simpan');
    $app->post('/update', \KriteriaController::class . ':update');
    $app->post('/hapus/{id}', \KriteriaController::class . ':hapus');
});

$app->group('/rule', function () use ($app) {
    $app->get('', \RuleController::class . ':index');
    $app->get('/tambah', \RuleController::class . ':tambah');
    $app->post('/simpan', \RuleController::class . ':simpan');
});

$app->group('/penilaian', function () use ($app) {
    $app->get('', \PenilaianController::class . ':index');
    $app->get('/tambah', \PenilaianController::class . ':tambah');
    $app->get('/edit/{id}', \PenilaianController::class . ':edit');
    $app->post('/simpan', \PenilaianController::class . ':simpan');
    $app->post('/update', \PenilaianController::class . ':update');
    $app->post('/hapus/{id}', \PenilaianController::class . ':hapus');
});

$app->group('/perhitungan', function () use ($app) {
    $app->get('', \PerhitunganController::class . ':index');
});
$app->group('/laporan', function () use ($app) {
    $app->get('', \LaporanController::class . ':index');
    $app->get('/print', \LaporanController::class . ':cetak');
});

$app->run();