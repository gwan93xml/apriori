<?php
/* Smarty version 3.1.31, created on 2017-09-01 19:19:32
  from "D:\xampp\htdocs\apriori\views\layout\main.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59a996a468f1f9_13007957',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c12183c57224600152b7b9e6e196626bec72f88d' => 
    array (
      0 => 'D:\\xampp\\htdocs\\apriori\\views\\layout\\main.tpl',
      1 => 1504286370,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59a996a468f1f9_13007957 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To | Bootstrap Based Admin Template - Material Design</title>
    <link rel="icon" href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/favicon.ico" type="image/x-icon">
	<link href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/css-font-roboto/index.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/material-icons-font/material-icons-font.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/node-waves/dist/waves.css" rel="stylesheet" />
	<link href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/animate.css/animate.css" rel="stylesheet" />
	<link href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/toastr/build/toastr.css" rel="stylesheet" />
	<link href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/assets/css/style.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/assets/css/themes/all-themes.css" rel="stylesheet" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/assets/waitme/waitMe.min.css" rel="stylesheet" />
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <div class="overlay"></div>
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html"><?php echo $_smarty_tpl->tpl_vars['appname']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['subappname']->value;?>
</a>
            </div>
        </div>
    </nav>
    <section>
        <aside id="leftsidebar" class="sidebar">
            <div class="user-info">
                <div class="image">
                    <img src="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/assets/images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $_smarty_tpl->tpl_vars['nama']->value;?>
</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li <?php if ($_smarty_tpl->tpl_vars['active']->value == 1) {?> class="active" <?php }?>>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
">
                            <i class="material-icons">home</i>
                            <span>Home</span>
                        </a>
                    </li>
                    <li <?php if ($_smarty_tpl->tpl_vars['active']->value == 2) {?> class="active" <?php }?>>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/barang">
                            <i class="material-icons">widgets</i>
                            <span>Data Barang</span>
                        </a>
                    </li>
                    <li <?php if ($_smarty_tpl->tpl_vars['active']->value == 3) {?> class="active" <?php }?>>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/penjualan">
                            <i class="material-icons">shopping_cart</i>
                            <span>Penjualan</span>
                        </a>
                    </li>
                    <li <?php if ($_smarty_tpl->tpl_vars['active']->value == 4) {?> class="active" <?php }?>>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/prosesapriori">
                            <i class="material-icons">low_priority</i>
                            <span>Proses Apriori</span>
                        </a>
                    </li>
                    <li <?php if ($_smarty_tpl->tpl_vars['active']->value == 5) {?> class="active" <?php }?>>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/penjualan">
                            <i class="material-icons">insert_drive_file</i>
                            <span>Laporan</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="legal">
                <div class="copyright">
                    &copy; 2017 <a href="javascript:void(0);"><?php echo $_smarty_tpl->tpl_vars['appname']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['subappname']->value;?>
</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.0
                </div>
            </div>
        </aside>
    </section>

	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/jquery/dist/jquery.min.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/bootstrap/dist/js/bootstrap.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/jquery-slimscroll/jquery.slimscroll.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/node-waves/dist/waves.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/toastr/build/toastr.min.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/jquery-validation/dist/jquery.validate.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/assets/js/admin.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/assets/js/pages/index.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/assets/waitme/waitMe.min.js"><?php echo '</script'; ?>
>
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3064659a996a46680f6_36842151', "content");
?>

</body>

</html><?php }
/* {block "content"} */
class Block_3064659a996a46680f6_36842151 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_3064659a996a46680f6_36842151',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php
}
}
/* {/block "content"} */
}
