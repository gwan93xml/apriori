<?php
/* Smarty version 3.1.31, created on 2017-09-01 15:21:29
  from "D:\xampp\htdocs\apriori\views\layout\block\header.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59a95ed97bd3a6_93554855',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2e36897e953cbefc694515dcceb5fefb60e0089a' => 
    array (
      0 => 'D:\\xampp\\htdocs\\apriori\\views\\layout\\block\\header.tpl',
      1 => 1503843355,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59a95ed97bd3a6_93554855 (Smarty_Internal_Template $_smarty_tpl) {
?>
<header class="main-header">
	<a href="./" class="logo">
		<span class="logo-mini"><b>FT</b></span>
		<span class="logo-lg">
		<b>
			Tsukamoto
		</b>
		</span>
	</a>
	<nav class="navbar navbar-static-top" role="navigation">
		<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img src="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/assets/img/administrator.png" class="user-image"/>
						<span class="hidden-xs"><?php echo $_smarty_tpl->tpl_vars['nama']->value;?>
</span>
					</a>
					<ul class="dropdown-menu">
						<li class="user-footer">
							<div class="pull-right">
								<a href="javascript:logout()" class="btn btn-default btn-flat">Logout</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
</header><?php }
}
