<?php
/* Smarty version 3.1.31, created on 2017-08-27 12:02:46
  from "D:\xampp\htdocs\tsukamoto\modules\PenilaianController\Views\tambah.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59a298c62548f3_59547589',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8a0cb3f637701f8ac0043116a03cd3d731878420' => 
    array (
      0 => 'D:\\xampp\\htdocs\\tsukamoto\\modules\\PenilaianController\\Views\\tambah.tpl',
      1 => 1503827970,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59a298c62548f3_59547589 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1000059a298c62179f6_27408366', "content");
$_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['app_tpl']->value);
}
/* {block "content"} */
class Block_1000059a298c62179f6_27408366 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_1000059a298c62179f6_27408366',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="box box-primary">
	<div class="box-header with-border">
		<div class="box-title">
			Tambah Data <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

		</div>
	</div>
	<div class="box-body">
		<form id="form-tambah-penilaian">
			<div class="col-md-6">
				<div class="form-group">
					<label>
						Customer :
					</label>
					<select class="form-control" name="customer">
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['customer']->value, 'item', false, 'key', 'name', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
?>
						<option value="<?php echo $_smarty_tpl->tpl_vars['item']->value['kodecustomer'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['namacustomer'];?>
</option>		
						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>
	
					</select>
				</div>
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>
								Kriteria
							</th>
							<th>
								Nilai
							</th>
						</tr>
					</thead>
					<tbody>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['kriteria']->value, 'item', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
?>
						<tr>
							<td>
								<?php echo $_smarty_tpl->tpl_vars['item']->value['namakriteria'];?>

							</td>
							<td>
								<?php if ($_smarty_tpl->tpl_vars['item']->value['useselect'] == 'Y') {?>
								<select class="form-control" name="nilai[<?php echo $_smarty_tpl->tpl_vars['item']->value['kodekriteria'];?>
]">
									<?php $_smarty_tpl->_assignInScope('nilai', 3);
?>
									<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['item']->value['subkriteria'], 'item1', false, 'key1');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key1']->value => $_smarty_tpl->tpl_vars['item1']->value) {
?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['nilai']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['item1']->value['label'];?>
</option>
									<?php $_smarty_tpl->_assignInScope('nilai', $_smarty_tpl->tpl_vars['nilai']->value+2);
?>
									<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

								</select>
								<?php } else { ?>
								<input class="form-control" name="nilai[<?php echo $_smarty_tpl->tpl_vars['item']->value['kodekriteria'];?>
]"></input>
								<?php }?>
							</td>
						</tr>
						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

					</tbody>
				</table>
			</div>
		</form>
	</div>

	<div class="box-footer with-border">
		<div class="box-tools pull-right">
			<div class="col-md-12">
				<button class="btn btn-primary" onclick="$('#form-tambah-penilaian').submit()"> <i class="fa fa-save"></i> Simpan </button>
				<a class="btn btn-default" href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/penilaian"> <i class="fa fa-align-justify"></i> Lihat Data </a>
			</div>
		</div>
	</div>
</div>
<?php echo '<script'; ?>
 type="text/javascript">
	$('#form-tambah-penilaian').submit(function(e){
		e.preventDefault();
		if(!confirm('Apakah anda ingin menyimpan data ini?')){
			return false;
		}
		$.ajax({
			url : '<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/penilaian/simpan',
			type : 'post',
			data : $('#form-tambah-penilaian').serialize(),
			success : function(response){
				if(response.success){
					toastr['success'](response.message);
					$('#form-tambah-penilaian')[0].reset();
				}
				else{
					toastr['error'](response.message);	
				}
			},
			error : function(){
				toastr['error']('Data gagal disimpan');
			}
		});
	});
<?php echo '</script'; ?>
>
<?php
}
}
/* {/block "content"} */
}
