<?php
/* Smarty version 3.1.31, created on 2017-08-22 17:26:35
  from "D:\xampp\htdocs\kost\modules\SearchController\Views\view.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_599c4d2b8325c0_01230154',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c50b0758ce754d71f71c61f08045c3f5f5278db8' => 
    array (
      0 => 'D:\\xampp\\htdocs\\kost\\modules\\SearchController\\Views\\view.tpl',
      1 => 1503415594,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_599c4d2b8325c0_01230154 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_27481599c4d2b7f56c4_26367761', "content");
$_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['app_tpl']->value);
}
/* {block "content"} */
class Block_27481599c4d2b7f56c4_26367761 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_27481599c4d2b7f56c4_26367761',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-search"></i> Hasil Pencarian
			</div>
			<div class="panel-body">
				<div class="col-md-12">
					<div class="row">
						
					<span class="pull-right">
						<select class="form-control input-sm">
							<option>Urutkan Berdasarkan</option>
							<option>Harga Terendah - Tertinggi</option>
							<option>Harga Tertinggi - Terendah</option>
							<option>Harga Terbaru - Terlama</option>
							<option>Harga Terlama - Terbaru</option>
						</select>
					</span>
					</div>
				</div>
				<div class="col-md-3 well">
					<small class="pull-right">
						Dilihat : 200x
					</small>
					<a href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
">
						<img class="img-thumbnail" src="https://apollo-singapore.akamaized.net/v1/files/nkqf16hqr21x2-ID/image;s=261x203;olx-st/_1_.jpg" style="width: 100%"/>
						<h4>
							Kontrakan Asri
						</h4>
					</a>
					<div class="col-md-12">
						<div class="row">
							Rp. 800.000/Bulan
						</div>
						<div class="row">
							<a href="">Kontrakan</a> » <a href="">Sitalasari</a> » <a href="">Setia Negara</a>
						</div>
					</div>
				</div>
				<div class="col-md-3 well">
					<small class="pull-right">
						Dilihat : 200x
					</small>
					<a href="">
						<img class="img-thumbnail" src="https://apollo-singapore.akamaized.net/v1/files/nkqf16hqr21x2-ID/image;s=261x203;olx-st/_1_.jpg" style="width: 100%"/>
						<h4>
							Kontrakan Asri
						</h4>
					</a>
					<div class="col-md-12">
						<div class="row">
							Rp. 800.000/Bulan
						</div>
						<div class="row">
							<a href="">Kontrakan</a> » <a href="">Sitalasari</a> » <a href="">Setia Negara</a>
						</div>
					</div>
				</div>
				<div class="col-md-3 well">
					<small class="pull-right">
						Dilihat : 200x
					</small>
					<a href="">
						<img class="img-thumbnail" src="https://apollo-singapore.akamaized.net/v1/files/nkqf16hqr21x2-ID/image;s=261x203;olx-st/_1_.jpg" style="width: 100%"/>
						<h4>
							Kontrakan Asri
						</h4>
					</a>
					<div class="col-md-12">
						<div class="row">
							Rp. 800.000/Bulan
						</div>
						<div class="row">
							<a href="">Kontrakan</a> » <a href="">Sitalasari</a> » <a href="">Setia Negara</a>
						</div>
					</div>
				</div>
				<div class="col-md-3 well">
					<small class="pull-right">
						Dilihat : 200x
					</small>
					<a href="">
						<img class="img-thumbnail" src="https://apollo-singapore.akamaized.net/v1/files/nkqf16hqr21x2-ID/image;s=261x203;olx-st/_1_.jpg" style="width: 100%"/>
						<h4>
							Kontrakan Asri
						</h4>
					</a>
					<div class="col-md-12">
						<div class="row">
							Rp. 800.000/Bulan
						</div>
						<div class="row">
							<a href="">Kontrakan</a> » <a href="">Sitalasari</a> » <a href="">Setia Negara</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo '<script'; ?>
 type="text/javascript">
	var kecamatanLoaded = false;
	var $kecamatanSelect = $("#kecamatan");
	$('#kecamatan').select2({
		placeholder : '-Pilih Kecamatan-'
	});
	$('#jenis').select2();
	$('#kelurahan').select2({
		placeholder: "-Pilih Kelurahan-",
		data: function() { 
			return{
				results: kelurahan
			}; 
		}
	});
	$kecamatanSelect.on("select2:open", function (e) {
		if(!kecamatanLoaded){
			loadKecamatan();
		} 
	});
	$kecamatanSelect.on("select2:select", function (e) { 
		loadKelurahan($(this).val());
	});

	function loadKecamatan(){
		$.ajax({
			url : '<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/kecamatan',
			type : 'POST',
			success : function(response){
				$('#kecamatan').empty();
				$('#kecamatan').select2('destroy');
				$.each(response,function(index,el){
					$('#kecamatan').append('<option value="'+el.id+'">'+el.nama_kecamatan+'</option>');	
				});
				$('#kecamatan').select2({
					placeholder : '-Pilih Kecamatan-'
				});
				kecamatanLoaded = true;
			},
			error : function(response){
				toastr['error'](response.responseText);
			}
		})
	}

	function loadKelurahan(idKecamatan){
		$.ajax({
			url : '<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/kelurahan',
			type : 'POST',
			data : {
				idKecamatan : idKecamatan
			},
			success : function(response){
				$('#kelurahan').empty();
				$('#kelurahan').select2('destroy');
				$.each(response,function(index,el){
					$('#kelurahan').append('<option value="'+el.id+'">'+el.nama_kelurahan+'</option>');	
				});
				$('#kelurahan').select2({
					placeholder : '-Pilih Kelurahan-'
				});
			},
			error : function(response){
				toastr['error'](response.responseText);
			}
		})
	}

<?php echo '</script'; ?>
>
<?php
}
}
/* {/block "content"} */
}
