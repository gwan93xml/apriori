<?php
/* Smarty version 3.1.31, created on 2017-08-22 15:47:57
  from "D:\xampp\htdocs\kost\views\layout\block\style.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_599c360dd288d8_52158658',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ee8a81240540a354bc15129db03baef8cf48fa27' => 
    array (
      0 => 'D:\\xampp\\htdocs\\kost\\views\\layout\\block\\style.tpl',
      1 => 1503409675,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_599c360dd288d8_52158658 (Smarty_Internal_Template $_smarty_tpl) {
?>
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/bootstrap/dist/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/bootstrap/dist/css/bootstrap-theme.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/font-awesome/css/font-awesome.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/inputmask/css/inputmask.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/select2/dist/css/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/toastr/build/toastr.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/source-sans-pro/source-sans-pro.css"/>
<style type="text/css">
	.logo-utama{
		height : 150px;
	}

	.well-danger {
		min-height: 20px;
		padding: 19px;
		margin-bottom: 20px;
		background-color: #f2dede;
		border: 1px solid #e3e3e3;
		border-radius: 4px;
		-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
	}
	.well-foot {
		padding: 19px;
		background-color: #f5f5f5;
		-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
	}.pace.pace-inactive {
  display: none;
}

.pace {
  -webkit-pointer-events: none;
  pointer-events: none;

  -webkit-user-select: none;
  -moz-user-select: none;
  user-select: none;

  z-index: 2000;
  position: fixed;
  height: 60px;
  width: 100px;
  margin: auto;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
}

.pace .pace-progress {
  z-index: 2000;
  position: absolute;
  height: 60px;
  width: 100px;

  -webkit-transform: translate3d(0, 0, 0) !important;
  -ms-transform: translate3d(0, 0, 0) !important;
  transform: translate3d(0, 0, 0) !important;
}

.pace .pace-progress:before {
  content: attr(data-progress-text);
  text-align: center;
  color: #fff;
  background: #29d;
  border-radius: 50%;
  font-family: "Helvetica Neue", sans-serif;
  font-size: 14px;
  font-weight: 100;
  line-height: 1;
  padding: 20% 0 7px;
  width: 50%;
  height: 40%;
  margin: 10px 0 0 30px;
  display: block;
  z-index: 999;
  position: absolute;
}

.pace .pace-activity {
  font-size: 15px;
  line-height: 1;
  z-index: 2000;
  position: absolute;
  height: 60px;
  width: 100px;

  display: block;
  -webkit-animation: pace-theme-center-atom-spin 2s linear infinite;
  -moz-animation: pace-theme-center-atom-spin 2s linear infinite;
  -o-animation: pace-theme-center-atom-spin 2s linear infinite;
  animation: pace-theme-center-atom-spin 2s linear infinite;
}

.pace .pace-activity {
  border-radius: 50%;
  border: 5px solid #29d;
  content: ' ';
  display: block;
  position: absolute;
  top: 0;
  left: 0;
  height: 60px;
  width: 100px;
}

.pace .pace-activity:after {
  border-radius: 50%;
  border: 5px solid #29d;
  content: ' ';
  display: block;
  position: absolute;
  top: -5px;
  left: -5px;
  height: 60px;
  width: 100px;

  -webkit-transform: rotate(60deg);
  -moz-transform: rotate(60deg);
  -o-transform: rotate(60deg);
  transform: rotate(60deg);
}

.pace .pace-activity:before {
  border-radius: 50%;
  border: 5px solid #29d;
  content: ' ';
  display: block;
  position: absolute;
  top: -5px;
  left: -5px;
  height: 60px;
  width: 100px;

  -webkit-transform: rotate(120deg);
  -moz-transform: rotate(120deg);
  -o-transform: rotate(120deg);
  transform: rotate(120deg);
}

@-webkit-keyframes pace-theme-center-atom-spin {
  0%   { -webkit-transform: rotate(0deg) }
  100% { -webkit-transform: rotate(359deg) }
}
@-moz-keyframes pace-theme-center-atom-spin {
  0%   { -moz-transform: rotate(0deg) }
  100% { -moz-transform: rotate(359deg) }
}
@-o-keyframes pace-theme-center-atom-spin {
  0%   { -o-transform: rotate(0deg) }
  100% { -o-transform: rotate(359deg) }
}
@keyframes pace-theme-center-atom-spin {
  0%   { transform: rotate(0deg) }
  100% { transform: rotate(359deg) }
}

</style><?php }
}
