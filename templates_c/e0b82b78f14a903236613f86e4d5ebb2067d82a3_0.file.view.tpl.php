<?php
/* Smarty version 3.1.31, created on 2017-08-22 16:56:54
  from "D:\xampp\htdocs\kost\modules\HomeController\Views\view.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_599c46363cdac8_46516749',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e0b82b78f14a903236613f86e4d5ebb2067d82a3' => 
    array (
      0 => 'D:\\xampp\\htdocs\\kost\\modules\\HomeController\\Views\\view.tpl',
      1 => 1503413813,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_599c46363cdac8_46516749 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14625599c46363cdac4_82043288', "content");
$_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['app_tpl']->value);
}
/* {block "content"} */
class Block_14625599c46363cdac4_82043288 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_14625599c46363cdac4_82043288',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="well">
	<div class="row">
		<form action="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/search">
			<div class="col-md-2">
				<select class="form-control select2" id="kecamatan" name="kecamatan">
				</select>
			</div>
			<div class="col-md-2">
				<select class="form-control select2" id="kelurahan" name="kelurahan">
				</select>
			</div>
			<div class="col-md-2">
				<select class="form-control select2" id="jenis" name="jenis">
					<option value="">Kontrakan/Kost</option>
					<option>Kontrakan</option>
					<option>Kost</option>
				</select>
			</div>
			<div class="col-md-6">
				<span class="input-group">
					<input class="form-control input-sm" placeholder="Cari...">
					<span class="input-group-btn">
						<button class="btn btn-danger btn-sm" type="submit">
							Cari <i class="fa fa-search"></i>
						</button>
					</span>
				</span>
			</div>
		</form>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-tag"></i> Terbaru
			</div>
			<div class="panel-body">
				<div class="col-md-3 well">
					<small class="pull-right">
						Dilihat : 200x
					</small>
					<a href="">
						<img class="img-thumbnail" src="https://apollo-singapore.akamaized.net/v1/files/nkqf16hqr21x2-ID/image;s=261x203;olx-st/_1_.jpg" style="width: 100%"/>
						<h4>
							Kontrakan Asri
						</h4>
					</a>
					<div class="col-md-12">
						<div class="row">
							Rp. 800.000/Bulan
						</div>
						<div class="row">
							<a href="">Kontrakan</a> » <a href="">Sitalasari</a> » <a href="">Setia Negara</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-danger">
			<div class="panel-heading">
				<i class="fa fa-star"></i>Terpopuler
			</div>
			<div class="panel-body">
				<div class="col-md-3 well-danger" >

					<small class="pull-right">
						Dilihat : 200x
					</small>
					<a href=""><img class="img-thumbnail" src="https://apollo-singapore.akamaized.net/v1/files/nkqf16hqr21x2-ID/image;s=261x203;olx-st/_1_.jpg" style="width: 100%"/>
						<h4>
							Kontrakan Asri
						</h4>
					</a>
					<div class="col-md-12">
						<div class="row">
							Rp. 800.000/Bulan
						</div>
						<div class="row">
							<a href="">Kontrakan</a> » <a href="">Sitalasari</a> » <a href="">Setia Negara</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo '<script'; ?>
 type="text/javascript">
	var kecamatanLoaded = false;
	var $kecamatanSelect = $("#kecamatan");
	$('#kecamatan').select2({
		placeholder : '-Pilih Kecamatan-'
	});
	$('#jenis').select2();
	$('#kelurahan').select2({
		placeholder: "-Pilih Kelurahan-",
		data: function() { 
			return{
				results: kelurahan
			}; 
		}
	});
	$kecamatanSelect.on("select2:open", function (e) {
		if(!kecamatanLoaded){
			loadKecamatan();
		} 
	});
	$kecamatanSelect.on("select2:select", function (e) { 
		loadKelurahan($(this).val());
	});

	function loadKecamatan(){
		$.ajax({
			url : '<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/kecamatan',
			type : 'POST',
			success : function(response){
				$('#kecamatan').empty();
				$('#kecamatan').select2('destroy');
				$.each(response,function(index,el){
					$('#kecamatan').append('<option value="'+el.id+'">'+el.nama_kecamatan+'</option>');	
				});
				$('#kecamatan').select2({
					placeholder : '-Pilih Kecamatan-'
				});
				kecamatanLoaded = true;
			},
			error : function(response){
				toastr['error'](response.responseText);
			}
		})
	}

	function loadKelurahan(idKecamatan){
		$.ajax({
			url : '<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/kelurahan',
			type : 'POST',
			data : {
				idKecamatan : idKecamatan
			},
			success : function(response){
				$('#kelurahan').empty();
				$('#kelurahan').select2('destroy');
				$.each(response,function(index,el){
					$('#kelurahan').append('<option value="'+el.id+'">'+el.nama_kelurahan+'</option>');	
				});
				$('#kelurahan').select2({
					placeholder : '-Pilih Kelurahan-'
				});
			},
			error : function(response){
				toastr['error'](response.responseText);
			}
		})
	}

<?php echo '</script'; ?>
>
<?php
}
}
/* {/block "content"} */
}
