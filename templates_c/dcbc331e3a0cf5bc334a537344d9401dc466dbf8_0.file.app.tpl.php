<?php
/* Smarty version 3.1.31, created on 2017-08-24 14:55:24
  from "D:\xampp\htdocs\tsukamoto\views\layout\app.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_599eccbc21f641_66630835',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'dcbc331e3a0cf5bc334a537344d9401dc466dbf8' => 
    array (
      0 => 'D:\\xampp\\htdocs\\tsukamoto\\views\\layout\\app.tpl',
      1 => 1503415621,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_599eccbc21f641_66630835 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
	<?php $_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['style']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

	<?php $_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['script']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-2">
			<a href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
">
				<img src="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/assets/img/logo.jpg" class="logo-utama pull-left">
			</a>
			</div>
			<div class="col-md-6">
				<h2 style="padding-top: 40px">Cari Cepat Kontakan &amp; Kost</h2>
			</div>
			<div class="col-md-4">
					<button class="btn btn-success pull-right">
						<i class="fa fa-plus"></i> Pasang Iklan
					</button>
					<button class="btn btn-default pull-right">
						<i class="fa fa-user"></i> My Profile
					</button>
			</div>
		</div>
		<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8201599eccbc21f640_09032864', "content");
?>

	</div>
	<div class="well-foot">
		<div class="container">
			<center>
				<small>
					Copyright &copy; 2017
				</small>
			</center>
		</div>
	</div>
</body>
</html><?php }
/* {block "content"} */
class Block_8201599eccbc21f640_09032864 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_8201599eccbc21f640_09032864',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block "content"} */
}
