<?php
/* Smarty version 3.1.31, created on 2017-08-27 15:56:39
  from "D:\xampp\htdocs\tsukamoto\modules\PerhitunganController\Views\view.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59a2cf973ebdd1_58810244',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '56a211f36b60a88d6f2fe889f314a3826f24e1c0' => 
    array (
      0 => 'D:\\xampp\\htdocs\\tsukamoto\\modules\\PerhitunganController\\Views\\view.tpl',
      1 => 1503842198,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59a2cf973ebdd1_58810244 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2008859a2cf973864c9_78454013', "content");
$_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['app_tpl']->value);
}
/* {block "content"} */
class Block_2008859a2cf973864c9_78454013 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_2008859a2cf973864c9_78454013',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="box box-primary">
	<div class="box-header with-border">
		<div class="box-title">
			Data <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

		</div>
		<div class="box-body">
			<form id="frm-perhitungan">
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['penilaian']->value, 'item', false, 'key', 'name', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
?>
				<?php echo $_smarty_tpl->tpl_vars['key']->value+1;?>
. <?php echo $_smarty_tpl->tpl_vars['item']->value['kodecustomer'];?>
 : <?php echo $_smarty_tpl->tpl_vars['item']->value['namacustomer'];?>

				<table class="table table-bordered">
					<tbody>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['item']->value['kriteria'], 'item1', false, 'key1');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key1']->value => $_smarty_tpl->tpl_vars['item1']->value) {
?>
						<tr>
							<td rowspan="<?php echo $_smarty_tpl->tpl_vars['item1']->value['total']+1;?>
" width="20%">
								<?php echo $_smarty_tpl->tpl_vars['item1']->value['kodekriteria'];?>
 : <?php echo $_smarty_tpl->tpl_vars['item1']->value['namakriteria'];?>

							</td>
						</tr>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['item1']->value['subkriteria'], 'item2', false, 'key2', 'name2', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key2']->value => $_smarty_tpl->tpl_vars['item2']->value) {
?>
						<tr>
							<td>
								<?php echo $_smarty_tpl->tpl_vars['item2']->value['subkriteria'];?>

							</td>
							<td>
								<?php echo $_smarty_tpl->tpl_vars['inferensi']->value[$_smarty_tpl->tpl_vars['item']->value['kodecustomer']][$_smarty_tpl->tpl_vars['item1']->value['kodekriteria']][$_smarty_tpl->tpl_vars['item2']->value['subkriteria']];?>

							</td>
						</tr>
						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

					</tbody>
				</table>

				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['item']->value['kriteria'], 'item1', false, 'key1');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key1']->value => $_smarty_tpl->tpl_vars['item1']->value) {
?>
							<th>
								<?php echo $_smarty_tpl->tpl_vars['item1']->value['namakriteria'];?>

							</th>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

							<th width="15%">
								a
							</th>
							<th  width="15%">
								z
							</th>
							<th  width="15%">
								a * z
							</th>
						</tr>
					</thead>
					<tbody>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['tsukamoto']->value[$_smarty_tpl->tpl_vars['item']->value['kodecustomer']], 'item1', false, 'key1');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key1']->value => $_smarty_tpl->tpl_vars['item1']->value) {
?>
						<tr>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['item1']->value, 'item2', false, 'key', 'name', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item2']->value) {
?>
						<td>
							<?php echo $_smarty_tpl->tpl_vars['item2']->value;?>

						</td>
						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

						<td>
							<?php echo $_smarty_tpl->tpl_vars['tsukamotohasil']->value[$_smarty_tpl->tpl_vars['item']->value['kodecustomer']][$_smarty_tpl->tpl_vars['key1']->value]['a'];?>

						</td>
						<td>
							<?php echo $_smarty_tpl->tpl_vars['tsukamotohasil']->value[$_smarty_tpl->tpl_vars['item']->value['kodecustomer']][$_smarty_tpl->tpl_vars['key1']->value]['z'];?>

						</td>
						<td>
							<?php echo $_smarty_tpl->tpl_vars['tsukamotohasil']->value[$_smarty_tpl->tpl_vars['item']->value['kodecustomer']][$_smarty_tpl->tpl_vars['key1']->value]['az'];?>

						</td>
						</tr>
						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

					</tbody>
				</table>
				Hasil : <?php echo $_smarty_tpl->tpl_vars['tsukamotohasil']->value[$_smarty_tpl->tpl_vars['item']->value['kodecustomer']]['hasil'];?>
 <br>
				Keterangan : <?php echo $_smarty_tpl->tpl_vars['tsukamotohasil']->value[$_smarty_tpl->tpl_vars['item']->value['kodecustomer']]['keterangan'];?>

				<br>
				<br>
				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

			</form>
		</div>
	</div>
	<?php echo '<script'; ?>
 type="text/javascript">
		$('#frm-rules').submit(function(e){
			e.preventDefault();
			if(!confirm('Apakah anda ingin menyimpan data ini?')){
				return false;
			}
			$.ajax({
				url : '<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/rule/simpan',
				type : 'post',
				data : $('#frm-rules').serialize(),
				success : function(response){
					if(response.success){
						toastr['success'](response.message);
						$('#form-tambah-kriteria')[0].reset();
					}
					else{
						toastr['error'](response.message);	
					}
				},
				error : function(){
					toastr['error']('Data gagal disimpan');
				}
			});
		});
	<?php echo '</script'; ?>
>
	<?php
}
}
/* {/block "content"} */
}
