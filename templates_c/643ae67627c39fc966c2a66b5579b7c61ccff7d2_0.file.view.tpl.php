<?php
/* Smarty version 3.1.31, created on 2017-09-01 18:23:19
  from "D:\xampp\htdocs\apriori\modules\CustomerController\Views\view.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59a98977580951_86612055',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '643ae67627c39fc966c2a66b5579b7c61ccff7d2' => 
    array (
      0 => 'D:\\xampp\\htdocs\\apriori\\modules\\CustomerController\\Views\\view.tpl',
      1 => 1503592352,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59a98977580951_86612055 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2284259a989775559c1_70711585', "content");
$_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['app_tpl']->value);
}
/* {block "content"} */
class Block_2284259a989775559c1_70711585 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_2284259a989775559c1_70711585',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="box box-primary">
	<div class="box-header with-border">
		<div class="box-title">
			Data <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

		</div>
		<div class="box-tools pull-right">
			<a href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/customer/tambah" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>
		</div>
	</div>
	<div class="box-body">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th width="1%">
						No. 
					</th>
					<th width="15%">
						Kode Customer
					</th>
					<th>
						Nama Customer
					</th>
					<th width="10%">
						Action
					</th>
				</tr>
			</thead>
			<tbody>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value, 'item', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
?>
				<tr>
					<td>
						<?php echo $_smarty_tpl->tpl_vars['key']->value+1;?>
.
					</td>
					<td>
						<?php echo $_smarty_tpl->tpl_vars['item']->value['kodecustomer'];?>

					</td>
					<td>
						<?php echo $_smarty_tpl->tpl_vars['item']->value['namacustomer'];?>

					</td>
					<td>
						<a class="btn btn-danger" href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/customer/hapus/<?php echo $_smarty_tpl->tpl_vars['item']->value['kodecustomer'];?>
" id="hapus">
							<i class="fa fa-trash">
							</i>
						</a>
						<a class="btn btn-warning" href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/customer/edit/<?php echo $_smarty_tpl->tpl_vars['item']->value['kodecustomer'];?>
">
							<i class="fa fa-pencil">
							</i>
						</a>
					</td>
				</tr>
				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

			</tbody>
			<tfoot>
				<tr>
					<td colspan="4">
						Total : <?php echo count($_smarty_tpl->tpl_vars['data']->value);?>
 Customer
					</td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>
<?php
}
}
/* {/block "content"} */
}
