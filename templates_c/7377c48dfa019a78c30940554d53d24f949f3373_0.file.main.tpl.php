<?php
/* Smarty version 3.1.31, created on 2017-08-24 18:03:32
  from "D:\xampp\htdocs\tsukamoto\views\layout\main.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_599ef8d49f4de2_98102609',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7377c48dfa019a78c30940554d53d24f949f3373' => 
    array (
      0 => 'D:\\xampp\\htdocs\\tsukamoto\\views\\layout\\main.tpl',
      1 => 1503590608,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:views/layout/block/style.tpl' => 1,
    'file:views/layout/block/script.tpl' => 1,
    'file:views/layout/block/header.tpl' => 1,
    'file:views/layout/block/menu.tpl' => 1,
    'file:views/layout/block/breadcrumb.tpl' => 1,
    'file:views/layout/block/footer.tpl' => 1,
  ),
),false)) {
function content_599ef8d49f4de2_98102609 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Fuzzy Tsukamoto</title>
	<?php $_smarty_tpl->_subTemplateRender("file:views/layout/block/style.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender("file:views/layout/block/script.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>
<body class="hold-transition skin-blue sidebar-mini fixed">
	<div id="wrapper">
		<?php $_smarty_tpl->_subTemplateRender("file:views/layout/block/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		<?php $_smarty_tpl->_subTemplateRender("file:views/layout/block/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		<div class="content-wrapper">
			<?php $_smarty_tpl->_subTemplateRender("file:views/layout/block/breadcrumb.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

			<section class="content">
				<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_31588599ef8d49f4de6_16264180', 'content');
?>

			</section>
			<?php $_smarty_tpl->_subTemplateRender("file:views/layout/block/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		</div>
	</div>
	<?php echo '<script'; ?>
 type="text/javascript">
		function logout(){
			if(!confirm('Apakah anda ingin logout?')){
				return false;
			}
			$.ajax({
				url : '<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/logout',
				success : function(){
					toastr['success']('Anda berhasil logout');
					window.location.replace('index.php');
				},
				error : function(response){
					toastr['error'](response.responseText);
				}
			});
		}

		$('body').on('click','#hapus',function(e){
			e.preventDefault();
			var link = $(this).attr('href');
			var tr = $(this).parents('tr');
			if(!confirm('Apakah anda ingin menghapus data ini?'))
				return false;
			$.ajax({
				url : link,
				type : 'POST',
				success : function(response){
					if(response.success){
						toastr['success']('Data berhasil dihapus');	
						tr.remove();
					}
					else{
						toastr['error'](response.message);	
					}
				},
				error : function(response){
					toastr['error'](response.responseText);
				}
			});
		});
	<?php echo '</script'; ?>
>
</body>
</html><?php }
/* {block 'content'} */
class Block_31588599ef8d49f4de6_16264180 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_31588599ef8d49f4de6_16264180',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'content'} */
}
