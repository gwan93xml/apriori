<?php
/* Smarty version 3.1.31, created on 2017-09-01 19:12:08
  from "D:\xampp\htdocs\apriori\modules\BarangController\Views\tambah.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59a994e87a5b43_00113004',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '742085b86e923853e7d302c3758cb0758d5566bf' => 
    array (
      0 => 'D:\\xampp\\htdocs\\apriori\\modules\\BarangController\\Views\\tambah.tpl',
      1 => 1504285922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59a994e87a5b43_00113004 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3105759a994e8796146_29559113', "content");
$_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['app_tpl']->value);
}
/* {block "content"} */
class Block_3105759a994e8796146_29559113 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_3105759a994e8796146_29559113',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


<section class="content">
	<div class="container-fluid">
		<div class="row clearfix">
			<div class="card">
				<div class="header">
					<h2>
						<?php echo $_smarty_tpl->tpl_vars['title']->value;?>
 <small>Tambah Data <?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</small>
					</h2>
					<ul class="header-dropdown m-r-0">
						<li>
							<button class="btn btn-primary" onclick="$('#form-tambah-barang').submit()"> <i class="material-icons" style="color: white">save</i> Simpan </button>
						</li>
						<li>
							<a class="btn btn-default" href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/barang"> <i class="material-icons" style="color: black">view_list</i> Lihat Data </a>
						</li>
					</ul>
				</div>
				<div class="body">
					
					<form id="form-tambah-barang">
						<div class="row clearfix">
							<div class="col-md-12">

								<div class="form-group form-float">

									<div class="form-line">

										<input class="form-control" name="kodebarang"></input>
										<label class="form-label">Kode Barang</label>	
									</div>
								</div>
								<div class="form-group form-float">
									<div class="form-line">
										<input class="form-control" name="namabarang"></input>
										<label class="form-label">Kode Barang</label>	
									</div>
								</div>
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<?php echo '<script'; ?>
 type="text/javascript">
	$('#form-tambah-barang').submit(function(e){
		e.preventDefault();
		if(!confirm('Apakah anda ingin menyimpan data ini?')){
			return false;
		}
		var effect = 'ios';
		var $loading = $('#form-tambah-barang').parents('.card').waitMe({
			effect: effect,
			text: 'Loading...',
			bg: 'rgba(255,255,255,0.90)',
			color: '#555'
		});
		$.ajax({
			url : '<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/barang/simpan',
			type : 'post',
			data : $('#form-tambah-barang').serialize(),
			complete : function(){
				$loading.waitMe('hide');
			},
			success : function(response){
				if(response.success){
					toastr['success'](response.message);
					$('#form-tambah-barang')[0].reset();
				}
				else{
					toastr['error'](response.message);	
				}
			},
			error : function(){
				toastr['error']('Data gagal disimpan');
			}
		});
	});
<?php echo '</script'; ?>
>
<?php
}
}
/* {/block "content"} */
}
