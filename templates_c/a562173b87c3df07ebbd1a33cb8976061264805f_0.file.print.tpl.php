<?php
/* Smarty version 3.1.31, created on 2017-08-27 16:15:23
  from "D:\xampp\htdocs\tsukamoto\modules\LaporanController\Views\print.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59a2d3fb8a6281_34666022',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a562173b87c3df07ebbd1a33cb8976061264805f' => 
    array (
      0 => 'D:\\xampp\\htdocs\\tsukamoto\\modules\\LaporanController\\Views\\print.tpl',
      1 => 1503843321,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59a2d3fb8a6281_34666022 (Smarty_Internal_Template $_smarty_tpl) {
?>
<title>
	Laporan
</title>
<table width="70%" align="center">
	<tr>
		<td width="10%">
			
		</td>
		<td align="center">
			<h3>
				SISTEM PENDUKUNG KEPUTUSAN KELAYAKAN KREDITUR SEPEDA MOTOR MENGGUNAKAN METODE FUZZY TSUKAMOTO
			</h3>
		</td>
		<td width="10%">
			
		</td>
	</tr>
</table>
<table border="1" width="70%" align="center">
	<thead>
		<tr>
			<th width="2%">
				No.
			</th>
			<th>
				Kode Customer
			</th>
			<th>
				Nama Customer
			</th>
			<th>
				Keterangan
			</th>
		</tr>
	</thead>
	<tbody>
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['hasil']->value, 'item', false, 'key', 'name', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
?>
		<tr>
			<td>
				<?php echo $_smarty_tpl->tpl_vars['key']->value+1;?>
.
			</td>
			<td>
				<?php echo $_smarty_tpl->tpl_vars['item']->value['kodecustomer'];?>

			</td>
			<td>
				<?php echo $_smarty_tpl->tpl_vars['item']->value['namacustomer'];?>

			</td>
			<td>
				<?php echo $_smarty_tpl->tpl_vars['item']->value['keterangan'];?>

			</td>
		</tr>
		<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

	</tbody>
</table>
<br>
<br>

<table width="15%" align="center">
	<tr>
		<td>
			Medan, .................. <br>
			Diketahui oleh <br>
			<br>
			<br>
			<br>

			(.......................) 
		</td>
	</tr>
</table>

	<?php echo '<script'; ?>
 type="text/javascript">
		window.print();
	<?php echo '</script'; ?>
><?php }
}
