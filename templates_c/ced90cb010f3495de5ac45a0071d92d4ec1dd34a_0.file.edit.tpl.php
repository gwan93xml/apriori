<?php
/* Smarty version 3.1.31, created on 2017-08-24 18:31:36
  from "D:\xampp\htdocs\tsukamoto\modules\CustomerController\Views\edit.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_599eff68390b14_92379103',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ced90cb010f3495de5ac45a0071d92d4ec1dd34a' => 
    array (
      0 => 'D:\\xampp\\htdocs\\tsukamoto\\modules\\CustomerController\\Views\\edit.tpl',
      1 => 1503592234,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_599eff68390b14_92379103 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13713599eff68390b14_55012367', "content");
$_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['app_tpl']->value);
}
/* {block "content"} */
class Block_13713599eff68390b14_55012367 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_13713599eff68390b14_55012367',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="box box-primary">
	<div class="box-header with-border">
		<div class="box-title">
			Edit Data <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

		</div>
	</div>
	<div class="box-body">
		<form id="form-edit-customer">
			<div class="col-md-6">
				<div class="form-group">
					<label>
						Kode Customer :
					</label>
					<input class="form-control" name="kodecustomer" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['kodecustomer'];?>
" readonly=""></input>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>
						Nama Customer :
					</label>
					<input class="form-control" name="namacustomer"  value="<?php echo $_smarty_tpl->tpl_vars['data']->value['namacustomer'];?>
"></input>
				</div>
			</div>
		</form>
	</div>

	<div class="box-footer with-border">
		<div class="box-tools pull-right">
			<div class="col-md-12">
				<button class="btn btn-warning" onclick="$('#form-edit-customer').submit()"> <i class="fa fa-pencil"></i> Update </button>
				<a class="btn btn-default" href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/customer"> <i class="fa fa-align-justify"></i> Lihat Data </a>
			</div>
		</div>
	</div>
</div>
<?php echo '<script'; ?>
 type="text/javascript">
	$('#form-edit-customer').submit(function(e){
		e.preventDefault();
		if(!confirm('Apakah anda ingin menyimpan data ini?')){
			return false;
		}
		$.ajax({
			url : '<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/customer/update',
			type : 'post',
			data : $('#form-edit-customer').serialize(),
			success : function(response){
				if(response.success){
					toastr['success'](response.message);
					window.location.replace('<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/customer');
				}
				else{
					toastr['error'](response.message);	
				}
			},
			error : function(){
				toastr['error']('Data gagal diupdate');
			}
		});
	});
<?php echo '</script'; ?>
>
<?php
}
}
/* {/block "content"} */
}
