<?php
/* Smarty version 3.1.31, created on 2017-08-27 16:20:46
  from "D:\xampp\htdocs\tsukamoto\modules\LaporanController\Views\view.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59a2d53e8cd502_60776023',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd2815acd6388b19feaf9c2889ff55bf7219836e1' => 
    array (
      0 => 'D:\\xampp\\htdocs\\tsukamoto\\modules\\LaporanController\\Views\\view.tpl',
      1 => 1503843317,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59a2d53e8cd502_60776023 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2882859a2d53e8a6408_31665552', "content");
$_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['app_tpl']->value);
}
/* {block "content"} */
class Block_2882859a2d53e8a6408_31665552 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_2882859a2d53e8a6408_31665552',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="box box-primary">
	<div class="box-header with-border">
		<div class="box-title">
			Data <?php echo $_smarty_tpl->tpl_vars['title']->value;?>


		</div>
<div class="box-tools pull-right">
			<a href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/laporan/print" class="btn btn-success" target="_blank"><i class="fa fa-print"></i> Print Data</a>
		</div>
		<div class="box-body">
			<table class="table table-bordered table-condensed">
				<thead>
					<tr>
						<th width="2%">
							No.
						</th>
						<th>
							Kode Customer
						</th>
						<th>
							Nama Customer
						</th>
						<th>
							Keterangan
						</th>
					</tr>
				</thead>
				<tbody>
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['hasil']->value, 'item', false, 'key', 'name', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
?>
						<tr>
							<td>
								<?php echo $_smarty_tpl->tpl_vars['key']->value+1;?>
.
							</td>
							<td>
								<?php echo $_smarty_tpl->tpl_vars['item']->value['kodecustomer'];?>

							</td>
							<td>
								<?php echo $_smarty_tpl->tpl_vars['item']->value['namacustomer'];?>

							</td>
							<td>
								<?php echo $_smarty_tpl->tpl_vars['item']->value['keterangan'];?>

							</td>
							<td>
								
							</td>
						</tr>
					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

				</tbody>
			</table>
		</div>
	</div>
	<?php
}
}
/* {/block "content"} */
}
