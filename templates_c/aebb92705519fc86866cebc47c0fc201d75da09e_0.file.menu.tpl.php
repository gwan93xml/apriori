<?php
/* Smarty version 3.1.31, created on 2017-08-27 15:57:23
  from "D:\xampp\htdocs\tsukamoto\views\layout\block\menu.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59a2cfc3bd1870_02830630',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'aebb92705519fc86866cebc47c0fc201d75da09e' => 
    array (
      0 => 'D:\\xampp\\htdocs\\tsukamoto\\views\\layout\\block\\menu.tpl',
      1 => 1503842242,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59a2cfc3bd1870_02830630 (Smarty_Internal_Template $_smarty_tpl) {
?>
<aside class="main-sidebar">
	<section class="sidebar">
		<div class="user-panel">
			<div class="pull-left image">
				<img src="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/assets/img/administrator.png" class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p><?php echo $_smarty_tpl->tpl_vars['nama']->value;?>
</p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<ul class="sidebar-menu" data-widget="tree">
			<li class="<?php if ($_smarty_tpl->tpl_vars['active']->value == 1) {?> active <?php }?>">
				<a  href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
">
					<i class="fa fa-home"></i>
					<span> Home</span>
				</a>
			</li>
			<li class="<?php if ($_smarty_tpl->tpl_vars['active']->value == 2) {?> active <?php }?>">
				<a  href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/customer" >
					<i class="fa fa-users"></i>
					<span>Data Customer</span>
				</a>
			</li>
			<li class="<?php if ($_smarty_tpl->tpl_vars['active']->value == 3) {?> active <?php }?>">
				<a  href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/kriteria" >
					<i class="fa fa-tag"></i>
					<span>Data Kriteria</span>
				</a>
			</li>
			<li class="<?php if ($_smarty_tpl->tpl_vars['active']->value == 4) {?> active <?php }?>">
				<a  href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/rule" >
					<i class="fa fa-dashboard"></i>
					<span>Data Rule</span>
				</a>
			</li>
			<li class="<?php if ($_smarty_tpl->tpl_vars['active']->value == 5) {?> active <?php }?>">
				<a  href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/penilaian" >
					<i class="fa fa-sort-numeric-desc"></i>
					<span>Penilaian</span>
				</a>
			</li>
			<li class="<?php if ($_smarty_tpl->tpl_vars['active']->value == 6) {?> active <?php }?>">
				<a  href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/perhitungan" >
					<i class="fa fa-check"></i>
					<span>Perhitungan</span>
				</a>
			</li>
			<li class="<?php if ($_smarty_tpl->tpl_vars['active']->value == 7) {?> active <?php }?>">
				<a  href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/laporan" >
					<i class="fa fa-file"></i>
					<span>Laporan</span>
				</a>
			</li>
		</ul>
	</section>
</aside><?php }
}
