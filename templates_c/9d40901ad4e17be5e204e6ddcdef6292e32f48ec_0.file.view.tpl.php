<?php
/* Smarty version 3.1.31, created on 2017-09-01 17:52:03
  from "D:\xampp\htdocs\apriori\modules\LoginController\Views\view.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59a982238fc252_24430993',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9d40901ad4e17be5e204e6ddcdef6292e32f48ec' => 
    array (
      0 => 'D:\\xampp\\htdocs\\apriori\\modules\\LoginController\\Views\\view.tpl',
      1 => 1504281119,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59a982238fc252_24430993 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html>
<head>
	<title>Login</title> 
	<link rel="icon" href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/favicon.ico" type="image/x-icon">
	<link href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/css-font-roboto/index.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/material-icons-font/material-icons-font.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/node-waves/dist/waves.css" rel="stylesheet" />
	<link href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/animate.css/animate.css" rel="stylesheet" />
	<link href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/toastr/build/toastr.css" rel="stylesheet" />
	<link href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/assets/css/style.css" rel="stylesheet">
</head>
<body class="login-page">
	<div class="login-box">
		<div class="logo">
			<a href="javascript:void(0);"><?php echo $_smarty_tpl->tpl_vars['appname']->value;?>
</a>
			<small><?php echo $_smarty_tpl->tpl_vars['subappname']->value;?>
</small>
		</div>
		<div class="card">
			<div class="body">
				<form id="frm-login" method="POST" onsubmit="return false">
					<div class="msg">Sign in to start your session</div>
					<div class="input-group">
						<span class="input-group-addon">
							<i class="material-icons">person</i>
						</span>
						<div class="form-line">
							<input type="text" class="form-control" name="Username" placeholder="Username" required autofocus>
						</div>
					</div>
					<div class="input-group">
						<span class="input-group-addon">
							<i class="material-icons">lock</i>
						</span>
						<div class="form-line">
							<input type="password" class="form-control" name="Password" placeholder="Password" required>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-8 p-t-5">
						</div>
						<div class="col-xs-4">
							<button class="btn btn-block bg-pink waves-effect" type="submit">SIGN IN</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/jquery/dist/jquery.min.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/bootstrap/dist/js/bootstrap.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/node-waves/dist/waves.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/toastr/build/toastr.min.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/node_modules/jquery-validation/dist/jquery.validate.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 type="text/javascript">
		$(function () {
			$('#frm-login').validate({
				highlight: function (input) {
					$(input).parents('.form-line').addClass('error');
				},
				unhighlight: function (input) {
					$(input).parents('.form-line').removeClass('error');
				},
				submitHandler : function(form){
					submitForm(form);
				},
				errorPlacement: function (error, element) {
					$(element).parents('.input-group').append(error);
				}
			});
		});
		function submitForm(form){
			$.ajax({
				url : '<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/login',
				data : $(form).serialize(),
				type : 'POST',
				success : function(response){
					if(response.success){
						toastr['success']('Anda berhasil login');
						window.location.replace('<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
');
					}
					else
					{
						toastr['error'](response.message);
					}
				},
				error : function(){
					toastr['error']('ERRORRR');
				}
			});
			return false;
		};
	<?php echo '</script'; ?>
>
</body>
</html><?php }
}
