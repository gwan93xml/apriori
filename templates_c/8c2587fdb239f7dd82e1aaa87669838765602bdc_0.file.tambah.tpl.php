<?php
/* Smarty version 3.1.31, created on 2017-08-24 18:16:11
  from "D:\xampp\htdocs\tsukamoto\modules\CustomerController\Views\tambah.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_599efbcb93ea42_12775711',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8c2587fdb239f7dd82e1aaa87669838765602bdc' => 
    array (
      0 => 'D:\\xampp\\htdocs\\tsukamoto\\modules\\CustomerController\\Views\\tambah.tpl',
      1 => 1503591124,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_599efbcb93ea42_12775711 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8936599efbcb93ea42_75918529', "content");
$_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['app_tpl']->value);
}
/* {block "content"} */
class Block_8936599efbcb93ea42_75918529 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_8936599efbcb93ea42_75918529',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="box box-primary">
	<div class="box-header with-border">
		<div class="box-title">
			Tambah Data <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

		</div>
	</div>
	<div class="box-body">
		<form id="form-tambah-customer">
			<div class="col-md-6">
				<div class="form-group">
					<label>
						Kode Customer :
					</label>
					<input class="form-control" name="kodecustomer"></input>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>
						Nama Customer :
					</label>
					<input class="form-control" name="namacustomer"></input>
				</div>
			</div>
		</form>
	</div>

	<div class="box-footer with-border">
		<div class="box-tools pull-right">
			<div class="col-md-12">
				<button class="btn btn-primary" onclick="$('#form-tambah-customer').submit()"> <i class="fa fa-save"></i> Simpan </button>
				<a class="btn btn-default" href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/customer"> <i class="fa fa-align-justify"></i> Lihat Data </a>
			</div>
		</div>
	</div>
</div>
<?php echo '<script'; ?>
 type="text/javascript">
	$('#form-tambah-customer').submit(function(e){
		e.preventDefault();
		if(!confirm('Apakah anda ingin menyimpan data ini?')){
			return false;
		}
		$.ajax({
			url : '<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/customer/simpan',
			type : 'post',
			data : $('#form-tambah-customer').serialize(),
			success : function(response){
				if(response.success){
					toastr['success'](response.message);
					$('#form-tambah-customer')[0].reset();
				}
				else{
					toastr['error'](response.message);	
				}
			},
			error : function(){
				toastr['error']('Data gagal disimpan');
			}
		});
	});
<?php echo '</script'; ?>
>
<?php
}
}
/* {/block "content"} */
}
