<?php
/* Smarty version 3.1.31, created on 2017-08-27 12:58:49
  from "D:\xampp\htdocs\tsukamoto\modules\PenilaianController\Views\edit.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59a2a5e9860d64_42894169',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e7ea36d9d30b0cd805f116e45a7f0bd48254e5eb' => 
    array (
      0 => 'D:\\xampp\\htdocs\\tsukamoto\\modules\\PenilaianController\\Views\\edit.tpl',
      1 => 1503831518,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59a2a5e9860d64_42894169 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_796559a2a5e9812b69_46644126', "content");
$_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['app_tpl']->value);
}
/* {block "content"} */
class Block_796559a2a5e9812b69_46644126 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_796559a2a5e9812b69_46644126',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="box box-primary">
	<div class="box-header with-border">
		<div class="box-title">
			Edit Data <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

		</div>
	</div>
	<div class="box-body">

		<form id="form-edit-penilaian">
			<div class="col-md-6">
				<div class="form-group">
					<label>
						Customer :
					</label>
					<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['data']->value[0]['kodecustomer'];?>
" name="kodecustomer">
					<input class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['data']->value[0]['namacustomer'];?>
" readonly=""></input>
				</div>
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>
								Kriteria
							</th>
							<th>
								Nilai
							</th>
						</tr>
					</thead>
					<tbody>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['kriteria']->value, 'item', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
?>
						<tr>
							<td>
								<?php echo $_smarty_tpl->tpl_vars['item']->value['namakriteria'];?>

							</td>
							<td>
								<?php if ($_smarty_tpl->tpl_vars['item']->value['useselect'] == 'Y') {?>
								<select class="form-control" name="nilai[<?php echo $_smarty_tpl->tpl_vars['item']->value['kodekriteria'];?>
]">
									<?php $_smarty_tpl->_assignInScope('nilai', 3);
?>
									<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['item']->value['subkriteria'], 'item1', false, 'key1');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key1']->value => $_smarty_tpl->tpl_vars['item1']->value) {
?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['nilai']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['nilai']->value == $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->tpl_vars['key']->value]['nilai']) {?> selected="" <?php }?>><?php echo $_smarty_tpl->tpl_vars['item1']->value['label'];?>
</option>
									<?php $_smarty_tpl->_assignInScope('nilai', $_smarty_tpl->tpl_vars['nilai']->value+2);
?>
									<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

								</select>
								<?php } else { ?>
								<input class="form-control" name="nilai[<?php echo $_smarty_tpl->tpl_vars['item']->value['kodekriteria'];?>
]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->tpl_vars['key']->value]['nilai'];?>
"></input>
								<?php }?>
							</td>
						</tr>
						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

					</tbody>
				</table>
			</div>
		</form>
	</div>

	<div class="box-footer with-border">
		<div class="box-tools pull-right">
			<div class="col-md-12">
				<button class="btn btn-warning" onclick="$('#form-edit-penilaian').submit()"> <i class="fa fa-pencil"></i> Update </button>
				<a class="btn btn-default" href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/penilaian"> <i class="fa fa-align-justify"></i> Lihat Data </a>
			</div>
		</div>
	</div>
</div>
<?php echo '<script'; ?>
 type="text/javascript">
	$('#form-edit-penilaian').submit(function(e){
		e.preventDefault();
		if(!confirm('Apakah anda ingin menyimpan data ini?')){
			return false;
		}
		$.ajax({
			url : '<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/penilaian/update',
			type : 'post',
			data : $('#form-edit-penilaian').serialize(),
			success : function(response){
				if(response.success){
					toastr['success'](response.message);
					window.location.replace('<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/penilaian');
				}
				else{
					toastr['error'](response.message);	
				}
			},
			error : function(){
				toastr['error']('Data gagal diupdate');
			}
		});
	});
<?php echo '</script'; ?>
>
<?php
}
}
/* {/block "content"} */
}
