<?php
/* Smarty version 3.1.31, created on 2017-08-26 15:19:26
  from "D:\xampp\htdocs\tsukamoto\modules\KriteriaController\Views\view.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59a1755e8b1269_67899917',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '417199d71b2be4666608b082ef0dd9619c45c758' => 
    array (
      0 => 'D:\\xampp\\htdocs\\tsukamoto\\modules\\KriteriaController\\Views\\view.tpl',
      1 => 1503753406,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59a1755e8b1269_67899917 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_772859a1755e837466_48799136', "content");
$_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['app_tpl']->value);
}
/* {block "content"} */
class Block_772859a1755e837466_48799136 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_772859a1755e837466_48799136',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="box box-primary">
	<div class="box-header with-border">
		<div class="box-title">
			Data <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

		</div>
		<div class="box-tools pull-right">
			<a href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/kriteria/tambah" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>
		</div>
	</div>
	<div class="box-body">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th width="1%">
						No. 
					</th>
					<th width="15%">
						Kode Kriteria
					</th>
					<th>
						Nama Kriteria
					</th>
					<th width="10%">
						Action
					</th>
				</tr>
			</thead>
			<tbody>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value, 'item', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
?>
				<tr>
					<td>
						<?php echo $_smarty_tpl->tpl_vars['key']->value+1;?>
.
					</td>
					<td>
						<?php echo $_smarty_tpl->tpl_vars['item']->value['kodekriteria'];?>

					</td>
					<td>
						<?php echo $_smarty_tpl->tpl_vars['item']->value['namakriteria'];?>

					</td>
					<td>
						<a class="btn btn-danger" href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/kriteria/hapus/<?php echo $_smarty_tpl->tpl_vars['item']->value['kodekriteria'];?>
" id="hapus">
							<i class="fa fa-trash">
							</i>
						</a>
						<a class="btn btn-warning" href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/kriteria/edit/<?php echo $_smarty_tpl->tpl_vars['item']->value['kodekriteria'];?>
">
							<i class="fa fa-pencil">
							</i>
						</a>
					</td>
				</tr>
				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

			</tbody>
			<tfoot>
				<tr>
					<td colspan="4">
						Total : <?php echo count($_smarty_tpl->tpl_vars['data']->value);?>
 Kriteria
					</td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>
<?php
}
}
/* {/block "content"} */
}
