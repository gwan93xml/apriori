<?php
/* Smarty version 3.1.31, created on 2017-09-01 19:15:14
  from "D:\xampp\htdocs\apriori\modules\BarangController\Views\view.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59a995a21d9556_76148130',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '728967d4b8e82b3009f7fc0ca588cd406ab53fd3' => 
    array (
      0 => 'D:\\xampp\\htdocs\\apriori\\modules\\BarangController\\Views\\view.tpl',
      1 => 1504286112,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59a995a21d9556_76148130 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1680659a995a21a68d3_42604937', "content");
$_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['app_tpl']->value);
}
/* {block "content"} */
class Block_1680659a995a21a68d3_42604937 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_1680659a995a21a68d3_42604937',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


<section class="content">
	<div class="container-fluid">
		<div class="row clearfix">
			<div class="card">
				<div class="header">
					<h2>
						<?php echo $_smarty_tpl->tpl_vars['title']->value;?>
 <small>View data <?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</small>
					</h2>
					<ul class="header-dropdown m-r-0">
						<li>
							<a href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/barang/tambah" class="btn btn-primary waves-effect"> <i class="material-icons" style="color: white">add</i> Tambah Data</a>
						</li>
					</ul>
				</div>

				<div class="body">
					<div class="row">
						
						<table class="table table-striped">
							<thead>
								<tr>
									<th width="1%">
										No. 
									</th>
									<th width="15%">
										Kode Barang
									</th>
									<th>
										Nama Barang
									</th>
									<th width="11%">
										Action
									</th>
								</tr>
							</thead>
							<tbody>
								<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value, 'item', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
?>
								<tr>
									<td>
										<?php echo $_smarty_tpl->tpl_vars['key']->value+1;?>
.
									</td>
									<td>
										<?php echo $_smarty_tpl->tpl_vars['item']->value['kodebarang'];?>

									</td>
									<td>
										<?php echo $_smarty_tpl->tpl_vars['item']->value['namabarang'];?>

									</td>
									<td>
										<a class="btn btn-danger" href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/barang/hapus/<?php echo $_smarty_tpl->tpl_vars['item']->value['kodebarang'];?>
" id="hapus">
										<i class="material-icons">delete
											</i>
										</a>
										<a class="btn btn-warning" href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/barang/edit/<?php echo $_smarty_tpl->tpl_vars['item']->value['kodebarang'];?>
">
											<i class="material-icons">mode_edit
											</i>
										</a>
									</td>
								</tr>
								<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

							</tbody>
							<tfoot>
								<tr>
									<td colspan="4">
										Total : <?php echo count($_smarty_tpl->tpl_vars['data']->value);?>
 <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

									</td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
}
}
/* {/block "content"} */
}
