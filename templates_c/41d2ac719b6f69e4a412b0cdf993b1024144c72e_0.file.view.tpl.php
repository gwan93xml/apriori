<?php
/* Smarty version 3.1.31, created on 2017-08-27 14:58:33
  from "D:\xampp\htdocs\tsukamoto\modules\RuleController\Views\view.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59a2c1f9509e45_83638545',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '41d2ac719b6f69e4a412b0cdf993b1024144c72e' => 
    array (
      0 => 'D:\\xampp\\htdocs\\tsukamoto\\modules\\RuleController\\Views\\view.tpl',
      1 => 1503836935,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59a2c1f9509e45_83638545 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_432959a2c1f9488fa3_73252155', "content");
$_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['app_tpl']->value);
}
/* {block "content"} */
class Block_432959a2c1f9488fa3_73252155 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_432959a2c1f9488fa3_73252155',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="box box-primary">
	<div class="box-header with-border">
		<div class="box-title">
			Data <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

		</div>
		<div class="box-tools pull-right">
			<button style="position: fixed; left :19%" class="btn btn-primary" onClick="$('#frm-rules').submit();"><i class="fa fa-save"></i> Simpan Rules</a>
		</div>
	</div>
	<div class="box-body">
		<form id="frm-rules">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>
							Rules 
						</th>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['kriteria']->value, 'item', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
?>
						<th>
							<?php echo $_smarty_tpl->tpl_vars['item']->value['namakriteria'];?>

						</th>
						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

						<th>
							Hasil
						</th>
					</tr>
				</thead>
				<tbody>
					<?php
$_smarty_tpl->tpl_vars['x'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);$_smarty_tpl->tpl_vars['x']->step = 1;$_smarty_tpl->tpl_vars['x']->total = (int) ceil(($_smarty_tpl->tpl_vars['x']->step > 0 ? $_smarty_tpl->tpl_vars['totalrule']->value+1 - (0) : 0-($_smarty_tpl->tpl_vars['totalrule']->value)+1)/abs($_smarty_tpl->tpl_vars['x']->step));
if ($_smarty_tpl->tpl_vars['x']->total > 0) {
for ($_smarty_tpl->tpl_vars['x']->value = 0, $_smarty_tpl->tpl_vars['x']->iteration = 1;$_smarty_tpl->tpl_vars['x']->iteration <= $_smarty_tpl->tpl_vars['x']->total;$_smarty_tpl->tpl_vars['x']->value += $_smarty_tpl->tpl_vars['x']->step, $_smarty_tpl->tpl_vars['x']->iteration++) {
$_smarty_tpl->tpl_vars['x']->first = $_smarty_tpl->tpl_vars['x']->iteration == 1;$_smarty_tpl->tpl_vars['x']->last = $_smarty_tpl->tpl_vars['x']->iteration == $_smarty_tpl->tpl_vars['x']->total;?>
					<tr>
						<td>
							Rule <?php echo $_smarty_tpl->tpl_vars['x']->value+1;?>

						</td>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['kriteria']->value, 'item', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
?>
						<td>
							<select name="R[<?php echo $_smarty_tpl->tpl_vars['x']->value+1;?>
][<?php echo $_smarty_tpl->tpl_vars['item']->value['kodekriteria'];?>
]" class="form-control">
								<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['item']->value['subkriteria'], 'item1', false, 'key1');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key1']->value => $_smarty_tpl->tpl_vars['item1']->value) {
?>
								<?php if ($_smarty_tpl->tpl_vars['item']->value['useselect'] == 'Y') {?>
								<option value="<?php echo $_smarty_tpl->tpl_vars['item1']->value['subkriteria'];?>
" <?php if (isset($_smarty_tpl->tpl_vars['dtrule']->value[$_smarty_tpl->tpl_vars['x']->value]['subrule'][$_smarty_tpl->tpl_vars['key']->value]['subkriteria'])) {
if ($_smarty_tpl->tpl_vars['dtrule']->value[$_smarty_tpl->tpl_vars['x']->value]['subrule'][$_smarty_tpl->tpl_vars['key']->value]['subkriteria'] == $_smarty_tpl->tpl_vars['item1']->value['subkriteria']) {?> selected <?php }
}?>><?php echo $_smarty_tpl->tpl_vars['item1']->value['label'];?>
</option>
								<?php } else { ?>
								<option value="<?php echo $_smarty_tpl->tpl_vars['item1']->value['subkriteria'];?>
" <?php if (isset($_smarty_tpl->tpl_vars['dtrule']->value[$_smarty_tpl->tpl_vars['x']->value]['subrule'][$_smarty_tpl->tpl_vars['key']->value]['subkriteria'])) {
if ($_smarty_tpl->tpl_vars['dtrule']->value[$_smarty_tpl->tpl_vars['x']->value]['subrule'][$_smarty_tpl->tpl_vars['key']->value]['subkriteria'] == $_smarty_tpl->tpl_vars['item1']->value['subkriteria']) {?> selected <?php }
}?>><?php echo $_smarty_tpl->tpl_vars['item1']->value['subkriteria'];?>
</option>
								<?php }?>
								<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

							</select>
						</td>
						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

						<td>
							<select name="Hasil[<?php echo $_smarty_tpl->tpl_vars['x']->value+1;?>
]" class="form-control">
								<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['hasil']->value['subkriteria'], 'item1', false, 'key1');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key1']->value => $_smarty_tpl->tpl_vars['item1']->value) {
?>
								<option value="<?php echo $_smarty_tpl->tpl_vars['item1']->value['subkriteria'];?>
" <?php if (isset($_smarty_tpl->tpl_vars['dtrule']->value[$_smarty_tpl->tpl_vars['x']->value]['hasil'])) {?> <?php if ($_smarty_tpl->tpl_vars['dtrule']->value[$_smarty_tpl->tpl_vars['x']->value]['hasil'] == $_smarty_tpl->tpl_vars['item1']->value['subkriteria']) {?> selected <?php }
}?>><?php echo $_smarty_tpl->tpl_vars['item1']->value['subkriteria'];?>
</option>
								<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

							</select>
						</td>
					</tr>
					<?php }
}
?>

				</tbody>
			</table>
		</form>
	</div>
</div>
<?php echo '<script'; ?>
 type="text/javascript">
	$('#frm-rules').submit(function(e){
		e.preventDefault();
		if(!confirm('Apakah anda ingin menyimpan data ini?')){
			return false;
		}
		$.ajax({
			url : '<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/rule/simpan',
			type : 'post',
			data : $('#frm-rules').serialize(),
			success : function(response){
				if(response.success){
					toastr['success'](response.message);
					$('#form-tambah-kriteria')[0].reset();
				}
				else{
					toastr['error'](response.message);	
				}
			},
			error : function(){
				toastr['error']('Data gagal disimpan');
			}
		});
	});
<?php echo '</script'; ?>
>
<?php
}
}
/* {/block "content"} */
}
