<?php
/* Smarty version 3.1.31, created on 2017-08-24 19:50:17
  from "D:\xampp\htdocs\tsukamoto\modules\KriteriaController\Views\tambah.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_599f11d967f214_52568872',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '46b919570be45db0c03813b39a6e38a781da360e' => 
    array (
      0 => 'D:\\xampp\\htdocs\\tsukamoto\\modules\\KriteriaController\\Views\\tambah.tpl',
      1 => 1503596934,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_599f11d967f214_52568872 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_21292599f11d967f212_37091013', "content");
$_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['app_tpl']->value);
}
/* {block "content"} */
class Block_21292599f11d967f212_37091013 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_21292599f11d967f212_37091013',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="box box-primary">
	<div class="box-header with-border">
		<div class="box-title">
			Tambah Data <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

		</div>
	</div>
	<div class="box-body">
		<form id="form-tambah-kriteria">
			<div class="col-md-6">
				<div class="form-group">
					<label>
						Kode Kriteria :
					</label>
					<input class="form-control" name="kodekriteria"></input>
				</div>
				<div class="form-group">
					<label>
						Use Select :
					</label>
					<select class="form-control" name="useselect">
						<option value="Y">Y</option>
						<option value="N">N</option>
					</select>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>
						Nama Kriteria :
					</label>
					<input class="form-control" name="namakriteria"></input>
				</div>

				<div class="form-group">
					<label>
						Hasil :
					</label>
					<select class="form-control" name="hasil">
						<option value="Y">Y</option>
						<option value="N">N</option>
					</select>
				</div>
			</div>
			<div class="col-md-12">
			<table class="table table-bordered" id="table-subkriteria">
				<thead>
					<tr>
						<th colspan="3">
						<button class="btn btn-success" id="btn-tambah-subkriteria" type="button"><i class="fa fa-plus"></i> Tambah Sub Kriteria</button>
						</th>
					</tr>
					<tr>
						<th width="5%">
							
						</th>
						<th width="30%">
							Sub Kriteria
						</th>
						<th width="30%">
							Range
						</th>
						<th width="30%">
							Label
						</th>
					</tr>
					<tbody>
						<?php $_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['vsubkriteria']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

					</tbody>
				</thead>
			</table>
			</div>
		</form>
	</div>
	<div class="box-footer with-border">
		<div class="box-tools pull-right">
			<div class="col-md-12">
				<button class="btn btn-primary" onclick="$('#form-tambah-kriteria').submit()"> <i class="fa fa-save"></i> Simpan </button>
				<a class="btn btn-default" href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/kriteria"> <i class="fa fa-align-justify"></i> Lihat Data </a>
			</div>
		</div>
	</div>
</div>
<?php echo '<script'; ?>
 type="text/javascript">
	var subkriteriitem = `<?php $_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['vsubkriteria']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
`;
	$('#form-tambah-kriteria').submit(function(e){
		e.preventDefault();
		if(!confirm('Apakah anda ingin menyimpan data ini?')){
			return false;
		}
		$.ajax({
			url : '<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/kriteria/simpan',
			type : 'post',
			data : $('#form-tambah-kriteria').serialize(),
			success : function(response){
				if(response.success){
					toastr['success'](response.message);
					$('#form-tambah-kriteria')[0].reset();
				}
				else{
					toastr['error'](response.message);	
				}
			},
			error : function(){
				toastr['error']('Data gagal disimpan');
			}
		});
	});
	$('#btn-tambah-subkriteria').click(function(e){
		$('#table-subkriteria tbody').append(subkriteriitem);
	});
	$('#table-subkriteria tbody').on('click','#btn-hapus-subkriteria', function(e){
		if($('#table-subkriteria tbody tr').length > 1){
			$(this).parents('tr').remove();
		} 
		else{
			toastr['error']('sub kriteria minimal 1 item');
		}
	});
<?php echo '</script'; ?>
>
<?php
}
}
/* {/block "content"} */
}
