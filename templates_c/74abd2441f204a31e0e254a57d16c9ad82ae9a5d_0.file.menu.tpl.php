<?php
/* Smarty version 3.1.31, created on 2017-09-01 15:21:29
  from "D:\xampp\htdocs\apriori\views\layout\block\menu.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59a95ed97ec1a2_17252989',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '74abd2441f204a31e0e254a57d16c9ad82ae9a5d' => 
    array (
      0 => 'D:\\xampp\\htdocs\\apriori\\views\\layout\\block\\menu.tpl',
      1 => 1503842242,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59a95ed97ec1a2_17252989 (Smarty_Internal_Template $_smarty_tpl) {
?>
<aside class="main-sidebar">
	<section class="sidebar">
		<div class="user-panel">
			<div class="pull-left image">
				<img src="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/assets/img/administrator.png" class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p><?php echo $_smarty_tpl->tpl_vars['nama']->value;?>
</p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<ul class="sidebar-menu" data-widget="tree">
			<li class="<?php if ($_smarty_tpl->tpl_vars['active']->value == 1) {?> active <?php }?>">
				<a  href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
">
					<i class="fa fa-home"></i>
					<span> Home</span>
				</a>
			</li>
			<li class="<?php if ($_smarty_tpl->tpl_vars['active']->value == 2) {?> active <?php }?>">
				<a  href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/customer" >
					<i class="fa fa-users"></i>
					<span>Data Customer</span>
				</a>
			</li>
			<li class="<?php if ($_smarty_tpl->tpl_vars['active']->value == 3) {?> active <?php }?>">
				<a  href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/kriteria" >
					<i class="fa fa-tag"></i>
					<span>Data Kriteria</span>
				</a>
			</li>
			<li class="<?php if ($_smarty_tpl->tpl_vars['active']->value == 4) {?> active <?php }?>">
				<a  href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/rule" >
					<i class="fa fa-dashboard"></i>
					<span>Data Rule</span>
				</a>
			</li>
			<li class="<?php if ($_smarty_tpl->tpl_vars['active']->value == 5) {?> active <?php }?>">
				<a  href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/penilaian" >
					<i class="fa fa-sort-numeric-desc"></i>
					<span>Penilaian</span>
				</a>
			</li>
			<li class="<?php if ($_smarty_tpl->tpl_vars['active']->value == 6) {?> active <?php }?>">
				<a  href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/perhitungan" >
					<i class="fa fa-check"></i>
					<span>Perhitungan</span>
				</a>
			</li>
			<li class="<?php if ($_smarty_tpl->tpl_vars['active']->value == 7) {?> active <?php }?>">
				<a  href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/laporan" >
					<i class="fa fa-file"></i>
					<span>Laporan</span>
				</a>
			</li>
		</ul>
	</section>
</aside><?php }
}
