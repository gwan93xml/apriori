<?php
/* Smarty version 3.1.31, created on 2017-08-24 19:49:58
  from "D:\xampp\htdocs\tsukamoto\modules\KriteriaController\Views\edit.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_599f11c6e0a402_30926132',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '725b405c86a746ce8e21d8ea29af0bc729b1235e' => 
    array (
      0 => 'D:\\xampp\\htdocs\\tsukamoto\\modules\\KriteriaController\\Views\\edit.tpl',
      1 => 1503596955,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_599f11c6e0a402_30926132 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13694599f11c6dcd506_12545507', "content");
$_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['app_tpl']->value);
}
/* {block "content"} */
class Block_13694599f11c6dcd506_12545507 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_13694599f11c6dcd506_12545507',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="box box-primary">
	<div class="box-header with-border">
		<div class="box-title">
			Edit Data <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

		</div>
	</div>
	<div class="box-body">
		<form id="form-edit-kriteria">
			<div class="col-md-6">
				<div class="form-group">
					<label>
						Kode Kriteria :
					</label>
					<input class="form-control" name="kodekriteria" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['kodekriteria'];?>
" readonly=""></input>
				</div>

				<div class="form-group">
					<label>
						Use Select :
					</label>
					<select class="form-control" name="useselect">
						<option value="Y" <?php if ($_smarty_tpl->tpl_vars['data']->value['useselect'] == 'Y') {?> selected="" <?php }?>>Y</option>
						<option value="N" <?php if ($_smarty_tpl->tpl_vars['data']->value['useselect'] == 'N') {?> selected="" <?php }?>>N</option>
					</select>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>
						Nama Kriteria :
					</label>
					<input class="form-control" name="namakriteria"  value="<?php echo $_smarty_tpl->tpl_vars['data']->value['namakriteria'];?>
"></input>
				</div>

				<div class="form-group">
					<label>
						Hasil :
					</label>
					<select class="form-control" name="hasil">
						<option value="Y" <?php if ($_smarty_tpl->tpl_vars['data']->value['hasil'] == 'Y') {?> selected="" <?php }?>>Y</option>
						<option value="N" <?php if ($_smarty_tpl->tpl_vars['data']->value['hasil'] == 'N') {?> selected="" <?php }?>>N</option>
					</select>
				</div>
			</div>

			<div class="col-md-12">
			<table class="table table-bordered" id="table-subkriteria">
				<thead>
					<tr>
						<th colspan="3">
						<button class="btn btn-success" id="btn-tambah-subkriteria" type="button"><i class="fa fa-plus"></i> Tambah Sub Kriteria</button>
						</th>
					</tr>
					<tr>
						<th width="5%">
							
						</th>
						<th width="30%">
							Sub Kriteria
						</th>
						<th width="30%">
							Range
						</th>
						<th width="30%">
							Label
						</th>
					</tr>
					<tbody>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['subkriteria']->value, 'item', false, 'key', 'name', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
?>
							<?php $_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['vsubkriteria']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('subkriteria'=>$_smarty_tpl->tpl_vars['item']->value), 0, true);
?>

						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

					</tbody>
				</thead>
			</table>
			</div>
		</form>
	</div>

	<div class="box-footer with-border">
		<div class="box-tools pull-right">
			<div class="col-md-12">
				<button class="btn btn-warning" onclick="$('#form-edit-kriteria').submit()"> <i class="fa fa-pencil"></i> Update </button>
				<a class="btn btn-default" href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/kriteria"> <i class="fa fa-align-justify"></i> Lihat Data </a>
			</div>
		</div>
	</div>
</div>
<?php echo '<script'; ?>
 type="text/javascript">

	var subkriteriitem = `<?php $_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['vsubkriteria']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
`;
	$('#form-edit-kriteria').submit(function(e){
		e.preventDefault();
		if(!confirm('Apakah anda ingin menyimpan data ini?')){
			return false;
		}
		$.ajax({
			url : '<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/kriteria/update',
			type : 'post',
			data : $('#form-edit-kriteria').serialize(),
			success : function(response){
				if(response.success){
					toastr['success'](response.message);
					window.location.replace('<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/kriteria');
				}
				else{
					toastr['error'](response.message);	
				}
			},
			error : function(){
				toastr['error']('Data gagal diupdate');
			}
		});
	});

	$('#btn-tambah-subkriteria').click(function(e){
		$('#table-subkriteria tbody').append(subkriteriitem);
	});
	$('#table-subkriteria tbody').on('click','#btn-hapus-subkriteria', function(e){
		if($('#table-subkriteria tbody tr').length > 1){
			$(this).parents('tr').remove();
		} 
		else{
			toastr['error']('sub kriteria minimal 1 item');
		}
	});
<?php echo '</script'; ?>
>
<?php
}
}
/* {/block "content"} */
}
