<?php
/* Smarty version 3.1.31, created on 2017-08-24 17:11:27
  from "D:\xampp\htdocs\tsukamoto\modules\LoginController\Views\view.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_599eec9faf4651_20364685',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cf7d4fed19b94a91a67bc6beafed518fe0b992e4' => 
    array (
      0 => 'D:\\xampp\\htdocs\\tsukamoto\\modules\\LoginController\\Views\\view.tpl',
      1 => 1503587485,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_599eec9faf4651_20364685 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<?php $_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['style']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

	<?php $_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['script']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

</head>

<body class="hold-transition login-page">
	<div class="login-box">
		<div class="login-logo">
			Silahkan Login
		</div>

		<div class="login-box-body">
			<form id="frm-login">
				<div class="form-group has-feedback">
					<label for="Username" class="active">Username</label>
					<input id="Username" class="form-control" type="text" name="Username">
					<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<label for="Password" class="active">Password</label>
					<input class="form-control" type="password" id="Password" name="Password">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="row">
					<div class="col-xs-4">
						<button type="submit" class="btn btn-primary btn-block btn-flat">
							<i class="fa fa-key"></i>
							Login
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<?php echo '<script'; ?>
 type="text/javascript">
		$('#frm-login').submit(function(e){
			e.preventDefault();
			$.ajax({
				url : '<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/login',
				data : $('#frm-login').serialize(),
				type : 'POST',
				success : function(response){
					if(response.success){
						toastr['success']('Anda berhasil login');
						window.location.replace('<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
');
					}
					else
					{
						toastr['error'](response.message);
					}
				},
				error : function(){

				}
			})
		});
	<?php echo '</script'; ?>
>
</body>
</html><?php }
}
