<?php
/* Smarty version 3.1.31, created on 2017-08-27 12:09:14
  from "D:\xampp\htdocs\tsukamoto\modules\PenilaianController\Views\view.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59a29a4af02bc8_87762132',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a7d4cf1a685853b635e9feb3877261f2fd525186' => 
    array (
      0 => 'D:\\xampp\\htdocs\\tsukamoto\\modules\\PenilaianController\\Views\\view.tpl',
      1 => 1503828553,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59a29a4af02bc8_87762132 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_238759a29a4aedf937_01095019', "content");
$_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['app_tpl']->value);
}
/* {block "content"} */
class Block_238759a29a4aedf937_01095019 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_238759a29a4aedf937_01095019',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="box box-primary">
	<div class="box-header with-border">
		<div class="box-title">
			Data <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

		</div>
		<div class="box-tools pull-right">
			<a href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/penilaian/tambah" class="btn btn-primary" ><i class="fa fa-plus"></i> Tambah Penilaian</a>
		</div>
	</div>
	<div class="box-body">
		<form id="frm-rules">
			<table class="table table-bordered">
				<thead>
					<tr>
					<th width="5%">
						No.
					</th>
						<th width="20%">
							Kode Customer 
						</th>
						<th>
							Nama Customer
						</th>	
						<th width="10%">
							Action
						</th>
					</tr>
				</thead>
				<tbody>
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value, 'item', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
?>
					<tr>
						<td>
							<?php echo $_smarty_tpl->tpl_vars['key']->value+1;?>
.
						</td>
						<td>
							<?php echo $_smarty_tpl->tpl_vars['item']->value['kodecustomer'];?>

						</td>
						<td>
							<?php echo $_smarty_tpl->tpl_vars['item']->value['namacustomer'];?>

						</td>
						<td>

							<a class="btn btn-danger" href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/penilaian/hapus/<?php echo $_smarty_tpl->tpl_vars['item']->value['kodecustomer'];?>
" id="hapus">
								<i class="fa fa-trash">
								</i>
							</a>
							<a class="btn btn-warning" href="<?php echo $_smarty_tpl->tpl_vars['root']->value;?>
/penilaian/edit/<?php echo $_smarty_tpl->tpl_vars['item']->value['kodecustomer'];?>
">
								<i class="fa fa-pencil">
								</i>
							</a>
						</td>
					</tr>
					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

				</tbody>
			</table>
		</form>
	</div>
</div>
<?php
}
}
/* {/block "content"} */
}
