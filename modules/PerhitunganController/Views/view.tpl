{extends file=$app_tpl}
{block name="content"}
<div class="box box-primary">
	<div class="box-header with-border">
		<div class="box-title">
			Data {$title}
		</div>
		<div class="box-body">
			<form id="frm-perhitungan">
				{foreach from=$penilaian item=item key=key name=name}
				{$key+1}. {$item.kodecustomer} : {$item.namacustomer}
				<table class="table table-bordered">
					<tbody>
						{foreach from=$item.kriteria item=item1 key=key1}
						<tr>
							<td rowspan="{$item1.total+1}" width="20%">
								{$item1.kodekriteria} : {$item1.namakriteria}
							</td>
						</tr>
						{foreach from=$item1['subkriteria'] item=item2 key=key2 name=name2}
						<tr>
							<td>
								{$item2.subkriteria}
							</td>
							<td>
								{$inferensi[$item['kodecustomer']][$item1['kodekriteria']][$item2['subkriteria']]}
							</td>
						</tr>
						{/foreach}
						{/foreach}
					</tbody>
				</table>

				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							{foreach from=$item.kriteria item=item1 key=key1}
							<th>
								{$item1.namakriteria}
							</th>
							{/foreach}
							<th width="15%">
								a
							</th>
							<th  width="15%">
								z
							</th>
							<th  width="15%">
								a * z
							</th>
						</tr>
					</thead>
					<tbody>
						{foreach from=$tsukamoto[$item['kodecustomer']] item=item1 key=key1}
						<tr>
						{foreach from=$item1 item=item2 key=key name=name}
						<td>
							{$item2}
						</td>
						{/foreach}
						<td>
							{$tsukamotohasil[$item['kodecustomer']][$key1]['a']}
						</td>
						<td>
							{$tsukamotohasil[$item['kodecustomer']][$key1]['z']}
						</td>
						<td>
							{$tsukamotohasil[$item['kodecustomer']][$key1]['az']}
						</td>
						</tr>
						{/foreach}
					</tbody>
				</table>
				Hasil : {$tsukamotohasil[$item['kodecustomer']]['hasil']} <br>
				Keterangan : {$tsukamotohasil[$item['kodecustomer']]['keterangan']}
				<br>
				<br>
				{/foreach}
			</form>
		</div>
	</div>
	<script type="text/javascript">
		$('#frm-rules').submit(function(e){
			e.preventDefault();
			if(!confirm('Apakah anda ingin menyimpan data ini?')){
				return false;
			}
			$.ajax({
				url : '{$root}/rule/simpan',
				type : 'post',
				data : $('#frm-rules').serialize(),
				success : function(response){
					if(response.success){
						toastr['success'](response.message);
						$('#form-tambah-kriteria')[0].reset();
					}
					else{
						toastr['error'](response.message);	
					}
				},
				error : function(){
					toastr['error']('Data gagal disimpan');
				}
			});
		});
	</script>
	{/block}