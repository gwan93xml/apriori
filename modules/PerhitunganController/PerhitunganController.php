<?php 
class PerhitunganController
{
    protected $smarty;
    protected $db;
    protected $vPath = 'modules/PerhitunganController/Views/';
    function __construct($container) {
     $this->smarty = $container->smarty;
     $this->db = $container->db;
     $this->smarty->assign('active', 6);
 }
 function index($request, $response, $args) {
    $penilaian = $this->db->getAll(' SELECT customer.kodecustomer, customer.namacustomer FROM penilaian INNER JOIN customer ON customer.kodecustomer=penilaian.kodecustomer group by customer.kodecustomer');
    foreach ($penilaian as $key => $value) {
        $penilaian[$key]['kriteria'] = $this->db->getAll(' SELECT *, (SELECT count(*) FROM subkriteria B  where B.kodekriteria=A.kodekriteria ) as total FROM penilaian A INNER JOIN kriteria B ON B.kodekriteria=A.kodekriteria where A.kodecustomer=? order by A.id',[$value['kodecustomer']]);
    }
    $nilai = $this->db->getAll(' SELECT * FROM penilaian order by id');
    foreach ($nilai as $key => $value) {
        $retnilai[$value['kodecustomer']][$value['kodekriteria']] = $value['nilai'];
    }
    foreach ($penilaian as $key => $value) {
        foreach ($value['kriteria'] as $key1 => $value1) {
            $penilaian[$key]['kriteria'][$key1]['subkriteria'] = $this->db->getAll(' SELECT * FROM subkriteria where kodekriteria=? order by id', [$value1['kodekriteria']]);
        }
    }
    foreach ($penilaian as $key => $value) {
        foreach ($value['kriteria'] as $key1 => $value1) {
            foreach ($value1['subkriteria'] as $key2 => $value2) {
                $nilai = 0;
                if($retnilai[$value['kodecustomer']][$value1['kodekriteria']] >= $value2['dari'] && $retnilai[$value['kodecustomer']][$value1['kodekriteria']] <= $value2['hingga']){
                    $nilai = 1;
                }
                $penilaian[$key]['kriteria'][$key1]['subkriteria'][$key2]['nilai'] = $nilai;
                $inferensi[$value['kodecustomer']][$value1['kodekriteria']][$value2['subkriteria']] = $nilai;
            }
        }
    }
    $rules = $this->db->getAll('SELECT * FROM rule');
    foreach ($rules as $key => $value) {
        $rules[$key]['subrule'] = $this->db->getAll(' SELECT * FROM subrule where koderule =? ',[$value['koderule']]);

    }
    foreach ($penilaian as $keyp => $valuep) {
        $totala = 0;
        $totalz = 0;
        foreach ($rules as $key => $value) {
            foreach ($value['subrule'] as $key1 => $value1) {
                $tsukamoto[$valuep['kodecustomer']][$value['koderule']][] = $inferensi[$valuep['kodecustomer']][$value1['kriteria']][$value1['subkriteria']];
                $a = min($tsukamoto[$valuep['kodecustomer']][$value['koderule']]);
                $bobot = $this->db->getRow(' SELECT hingga FROM subkriteria INNER JOIN kriteria where subkriteria=? and hasil="Y"',[$value['hasil']])['hingga'];
                $tsukamotohasil[$valuep['kodecustomer']][$value['koderule']]['a'] = $a;
                $tsukamotohasil[$valuep['kodecustomer']][$value['koderule']]['z'] = $a * $bobot;
            }
        } 
    }
    foreach ($tsukamotohasil as $key => $value) {
        $totala = 0;
        $totalaz = 0;
        foreach ($value as $key1 => $value1) {
            $tsukamotohasil[$key][$key1]['az'] = $value1['a'] * $value1['z'];
            $totala += $value1['a'];
            $totalaz += $value1['a'] * $value1['z'];
        }
        $tsukamotohasil[$key]['hasil'] = $totalaz/$totala;
        $tsukamotohasil[$key]['keterangan'] = $this->db->getRow(' SELECT subkriteria, kriteria.kodekriteria FROM subkriteria INNER JOIN kriteria ON kriteria.kodekriteria = subkriteria.kodekriteria where kriteria.hasil="Y" AND subkriteria.hingga=? ',[$totalaz/$totala])['subkriteria'];          
    }
    foreach ($penilaian as $key => $value) {
        $hasil[$key] = $this->db->dispense('hasil');
        $hasil[$key]->kodecustomer = $value['kodecustomer'];
        $hasil[$key]->namacustomer = $value['namacustomer'];
        $hasil[$key]->keterangan = $tsukamotohasil[$value['kodecustomer']]['keterangan'];
    }
    $this->db->exec("DELETE FROM hasil");
    $this->db->storeAll($hasil);
    $this->smarty->assign('inferensi', $inferensi);
    $this->smarty->assign('penilaian', $penilaian);
    $this->smarty->assign('tsukamoto', $tsukamoto);
    $this->smarty->assign('tsukamotohasil', $tsukamotohasil);
    $this->smarty->assign('title','Perhitungan');
    return $this->smarty->display($this->vPath.'view.tpl');
}
function simpan($request, $response, $args) {
    $rules = $request->getParams()['R'];
    $hasil = $request->getParams()['Hasil'];
    $this->db->exec(' DELETE FROM rule ');
    $this->db->exec(' DELETE FROM subrule ');
    foreach ($rules as $key => $value) {
        $dtrules[$key] = $this->db->dispense('rule');
        $dtrules[$key]->koderule = 'R'.$key;
        foreach ($value as $key1 => $value1) {
            $dtsubrules[$key." ".$key1] = $this->db->dispense('subrule');
            $dtsubrules[$key." ".$key1]->koderule = "R".$key;
            $dtsubrules[$key." ".$key1]->kriteria = $key1;
            $dtsubrules[$key." ".$key1]->subkriteria = $value1;
        }
        $dtrules[$key]->hasil = $hasil[$key];
    }
    $this->db->storeAll($dtsubrules);
    $this->db->storeAll($dtrules);
    $res['success'] = true;
    $res['message'] = 'Data rules berhasil disimpan' ;        
    return $response->withJson($res);
}
function update($request, $response, $args) {
    $kodecustomer = $request->getParams()['kodecustomer'];
    $namacustomer = $request->getParams()['namacustomer'];
    $customer = $this->db->findOne('customer','kodecustomer = ?', [$kodecustomer]);
    if(!count($customer)){
        $res['success'] = false;
        $res['message'] = 'Maaf data dengan kode customer ini tidak ada';
        return $response->withJson($res);
    }
    $customer->namacustomer = $namacustomer;
    $this->db->store($customer);
    $res['success'] = true;
    $res['message'] = 'Data customer berhasil diupdate' ;        
    return $response->withJson($res);
}
function hapus($request, $response, $args) {
    $customer = $this->db->findOne('customer','kodecustomer = ?', [$args['id']]);
    $this->db->trash($customer);
    $res['success'] = true;
    $res['message'] = 'Data customer berhasil dihapus' ;        
    return $response->withJson($res);
}
}