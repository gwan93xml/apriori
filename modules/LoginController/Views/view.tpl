<!DOCTYPE html>
<html>
<head>
	<title>Login</title> 
	<link rel="icon" href="{$root}/favicon.ico" type="image/x-icon">
	<link href="{$root}/node_modules/css-font-roboto/index.css" rel="stylesheet" type="text/css">
	<link href="{$root}/node_modules/material-icons-font/material-icons-font.css" rel="stylesheet" type="text/css">
	<link href="{$root}/node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
	<link href="{$root}/node_modules/node-waves/dist/waves.css" rel="stylesheet" />
	<link href="{$root}/node_modules/animate.css/animate.css" rel="stylesheet" />
	<link href="{$root}/node_modules/toastr/build/toastr.css" rel="stylesheet" />
	<link href="{$root}/assets/css/style.css" rel="stylesheet">
</head>
<body class="login-page">
	<div class="login-box">
		<div class="logo">
			<a href="javascript:void(0);">{$appname}</a>
			<small>{$subappname}</small>
		</div>
		<div class="card">
			<div class="body">
				<form id="frm-login" method="POST" onsubmit="return false">
					<div class="msg">Sign in to start your session</div>
					<div class="input-group">
						<span class="input-group-addon">
							<i class="material-icons">person</i>
						</span>
						<div class="form-line">
							<input type="text" class="form-control" name="Username" placeholder="Username" required autofocus>
						</div>
					</div>
					<div class="input-group">
						<span class="input-group-addon">
							<i class="material-icons">lock</i>
						</span>
						<div class="form-line">
							<input type="password" class="form-control" name="Password" placeholder="Password" required>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-8 p-t-5">
						</div>
						<div class="col-xs-4">
							<button class="btn btn-block bg-pink waves-effect" type="submit">SIGN IN</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script src="{$root}/node_modules/jquery/dist/jquery.min.js"></script>
	<script src="{$root}/node_modules/bootstrap/dist/js/bootstrap.js"></script>
	<script src="{$root}/node_modules/node-waves/dist/waves.js"></script>
	<script src="{$root}/node_modules/toastr/build/toastr.min.js"></script>
	<script src="{$root}/node_modules/jquery-validation/dist/jquery.validate.js"></script>
	<script type="text/javascript">
		$(function () {
			$('#frm-login').validate({
				highlight: function (input) {
					$(input).parents('.form-line').addClass('error');
				},
				unhighlight: function (input) {
					$(input).parents('.form-line').removeClass('error');
				},
				submitHandler : function(form){
					submitForm(form);
				},
				errorPlacement: function (error, element) {
					$(element).parents('.input-group').append(error);
				}
			});
		});
		function submitForm(form){
			$.ajax({
				url : '{$root}/login',
				data : $(form).serialize(),
				type : 'POST',
				success : function(response){
					if(response.success){
						toastr['success']('Anda berhasil login');
						window.location.replace('{$root}');
					}
					else
					{
						toastr['error'](response.message);
					}
				},
				error : function(){
					toastr['error']('ERRORRR');
				}
			});
			return false;
		};
	</script>
</body>
</html>