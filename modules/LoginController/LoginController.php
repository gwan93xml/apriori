<?php 
class LoginController{
    protected $smarty;
    protected $db;
    protected $vPath = 'modules/LoginController/Views/';
    function __construct($container) {
       $this->smarty = $container->smarty;
       $this->db = $container->db;
   }
   function index($request, $response, $args) {
     $kecamatan = $this->db->getAll('SELECT * FROM kecamatan');
     $this->smarty->assign('kecamatan',$kecamatan);
     $this->smarty->assign('title','Pencarian Kost');
     return $this->smarty->display($this->vPath.'view.tpl');
 }
 function loginAction($request, $response, $args){
    $username = $request->getParams('Username')['Username'];
    $password = $request->getParams('Password')['Password'];
    if($username == "" || $password == ""){
        $res['success'] = false;
        $res['message'] = 'Silahkan isi username dan password';
        return $response->withJson($res);
    }
    $login = $this->db->getRow('SELECT * FROM login WHERE username = ? and password = ? ',[$username,$password]);
    if(count($login)){
        $_SESSION['username'] = $username;
        $_SESSION['nama'] = $login['nama'];
        $res['success'] = true;
        return $response->withJson($res);
    }
    else{
        $res['success'] = false;
        $res['message'] = 'Username atau password salah';
        return $response->withJson($res);
    }
}
 function logout($request, $response, $args){
    unset($_SESSION['username']);
}
}