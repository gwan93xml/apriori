<?php 
class SearchController
{
    protected $smarty;
    protected $db;
    protected $vPath = 'modules/SearchController/Views/';
    function __construct($container) {
      	$this->smarty = $container->smarty;
      	$this->db = $container->db;
    }
    function view($request, $response, $args) {
    	$kecamatan = $this->db->getAll('SELECT * FROM kecamatan');
    	$this->smarty->assign('kecamatan',$kecamatan);
    	$this->smarty->assign('title','Pencarian Kost');
    	return $this->smarty->display($this->vPath.'view.tpl');
    }
}