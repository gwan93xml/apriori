{extends file=$app_tpl}
{block name="content"}
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-search"></i> Hasil Pencarian
			</div>
			<div class="panel-body">
				<div class="col-md-12">
					<div class="row">
						
					<span class="pull-right">
						<select class="form-control input-sm">
							<option>Urutkan Berdasarkan</option>
							<option>Harga Terendah - Tertinggi</option>
							<option>Harga Tertinggi - Terendah</option>
							<option>Harga Terbaru - Terlama</option>
							<option>Harga Terlama - Terbaru</option>
						</select>
					</span>
					</div>
				</div>
				<div class="col-md-3 well">
					<small class="pull-right">
						Dilihat : 200x
					</small>
					<a href="{$root}">
						<img class="img-thumbnail" src="https://apollo-singapore.akamaized.net/v1/files/nkqf16hqr21x2-ID/image;s=261x203;olx-st/_1_.jpg" style="width: 100%"/>
						<h4>
							Kontrakan Asri
						</h4>
					</a>
					<div class="col-md-12">
						<div class="row">
							Rp. 800.000/Bulan
						</div>
						<div class="row">
							<a href="">Kontrakan</a> » <a href="">Sitalasari</a> » <a href="">Setia Negara</a>
						</div>
					</div>
				</div>
				<div class="col-md-3 well">
					<small class="pull-right">
						Dilihat : 200x
					</small>
					<a href="">
						<img class="img-thumbnail" src="https://apollo-singapore.akamaized.net/v1/files/nkqf16hqr21x2-ID/image;s=261x203;olx-st/_1_.jpg" style="width: 100%"/>
						<h4>
							Kontrakan Asri
						</h4>
					</a>
					<div class="col-md-12">
						<div class="row">
							Rp. 800.000/Bulan
						</div>
						<div class="row">
							<a href="">Kontrakan</a> » <a href="">Sitalasari</a> » <a href="">Setia Negara</a>
						</div>
					</div>
				</div>
				<div class="col-md-3 well">
					<small class="pull-right">
						Dilihat : 200x
					</small>
					<a href="">
						<img class="img-thumbnail" src="https://apollo-singapore.akamaized.net/v1/files/nkqf16hqr21x2-ID/image;s=261x203;olx-st/_1_.jpg" style="width: 100%"/>
						<h4>
							Kontrakan Asri
						</h4>
					</a>
					<div class="col-md-12">
						<div class="row">
							Rp. 800.000/Bulan
						</div>
						<div class="row">
							<a href="">Kontrakan</a> » <a href="">Sitalasari</a> » <a href="">Setia Negara</a>
						</div>
					</div>
				</div>
				<div class="col-md-3 well">
					<small class="pull-right">
						Dilihat : 200x
					</small>
					<a href="">
						<img class="img-thumbnail" src="https://apollo-singapore.akamaized.net/v1/files/nkqf16hqr21x2-ID/image;s=261x203;olx-st/_1_.jpg" style="width: 100%"/>
						<h4>
							Kontrakan Asri
						</h4>
					</a>
					<div class="col-md-12">
						<div class="row">
							Rp. 800.000/Bulan
						</div>
						<div class="row">
							<a href="">Kontrakan</a> » <a href="">Sitalasari</a> » <a href="">Setia Negara</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var kecamatanLoaded = false;
	var $kecamatanSelect = $("#kecamatan");
	$('#kecamatan').select2({
		placeholder : '-Pilih Kecamatan-'
	});
	$('#jenis').select2();
	$('#kelurahan').select2({
		placeholder: "-Pilih Kelurahan-",
		data: function() { 
			return{
				results: kelurahan
			}; 
		}
	});
	$kecamatanSelect.on("select2:open", function (e) {
		if(!kecamatanLoaded){
			loadKecamatan();
		} 
	});
	$kecamatanSelect.on("select2:select", function (e) { 
		loadKelurahan($(this).val());
	});

	function loadKecamatan(){
		$.ajax({
			url : '{$root}/kecamatan',
			type : 'POST',
			success : function(response){
				$('#kecamatan').empty();
				$('#kecamatan').select2('destroy');
				$.each(response,function(index,el){
					$('#kecamatan').append('<option value="'+el.id+'">'+el.nama_kecamatan+'</option>');	
				});
				$('#kecamatan').select2({
					placeholder : '-Pilih Kecamatan-'
				});
				kecamatanLoaded = true;
			},
			error : function(response){
				toastr['error'](response.responseText);
			}
		})
	}

	function loadKelurahan(idKecamatan){
		$.ajax({
			url : '{$root}/kelurahan',
			type : 'POST',
			data : {
				idKecamatan : idKecamatan
			},
			success : function(response){
				$('#kelurahan').empty();
				$('#kelurahan').select2('destroy');
				$.each(response,function(index,el){
					$('#kelurahan').append('<option value="'+el.id+'">'+el.nama_kelurahan+'</option>');	
				});
				$('#kelurahan').select2({
					placeholder : '-Pilih Kelurahan-'
				});
			},
			error : function(response){
				toastr['error'](response.responseText);
			}
		})
	}

</script>
{/block}