<?php 
class BarangController
{
    protected $smarty;
    protected $db;
    protected $vPath = 'modules/BarangController/Views/';
    function __construct($container) {
      	$this->smarty = $container->smarty;
      	$this->db = $container->db;
        $this->smarty->assign('active', 2);
    }
    function index($request, $response, $args) {
        $data = $this->db->getAll('SELECT * FROM barang');
        $this->smarty->assign('data',$data);
        $this->smarty->assign('title','Barang');
        return $this->smarty->display($this->vPath.'view.tpl');
    }
    function tambah($request, $response, $args) {
        $this->smarty->assign('title','Barang');
        return $this->smarty->display($this->vPath.'tambah.tpl');
    }
    function edit($request, $response, $args) {
        $data = $this->db->findOne('barang','kodebarang = ?', [$args['id']]);
        $this->smarty->assign('data',$data);
        $this->smarty->assign('title','Barang');
        return $this->smarty->display($this->vPath.'edit.tpl');
    }
    function simpan($request, $response, $args) {
        $kodebarang = $request->getParams()['kodebarang'];
        $namabarang = $request->getParams()['namabarang'];
        $cek = $this->db->findOne('barang','kodebarang = ?', [$kodebarang]);
        if(count($cek)){
            $res['success'] = false;
            $res['message'] = 'Maaf data dengan kode barang ini sudah ada';
            return $response->withJson($res);
        }
        $barang = $this->db->dispense('barang');
        $barang->kodebarang = $kodebarang;
        $barang->namabarang = $namabarang;
        $this->db->store($barang);
        $res['success'] = true;
        $res['message'] = 'Data barang berhasil disimpan' ;        
        return $response->withJson($res);
    }
    function update($request, $response, $args) {
        $kodebarang = $request->getParams()['kodebarang'];
        $namabarang = $request->getParams()['namabarang'];
        $barang = $this->db->findOne('barang','kodebarang = ?', [$kodebarang]);
        if(!count($barang)){
            $res['success'] = false;
            $res['message'] = 'Maaf data dengan kode barang ini tidak ada';
            return $response->withJson($res);
        }
        $barang->namabarang = $namabarang;
        $this->db->store($barang);
        $res['success'] = true;
        $res['message'] = 'Data barang berhasil diupdate' ;        
        return $response->withJson($res);
    }
    function hapus($request, $response, $args) {
        $barang = $this->db->findOne('barang','kodebarang = ?', [$args['id']]);
        $this->db->trash($barang);
        $res['success'] = true;
        $res['message'] = 'Data barang berhasil dihapus' ;        
        return $response->withJson($res);
    }
}