{extends file=$app_tpl}
{block name="content"}

<section class="content">
	<div class="container-fluid">
		<div class="row clearfix">
			<div class="card">
				<div class="header">
					<h2>
						{$title} <small>View data {$title}</small>
					</h2>
					<ul class="header-dropdown m-r-0">
						<li>
							<a href="{$root}/barang/tambah" class="btn btn-primary waves-effect"> <i class="material-icons" style="color: white">add</i> Tambah Data</a>
						</li>
					</ul>
				</div>

				<div class="body">
					<div class="row">
						
						<table class="table table-striped">
							<thead>
								<tr>
									<th width="1%">
										No. 
									</th>
									<th width="15%">
										Kode Barang
									</th>
									<th>
										Nama Barang
									</th>
									<th width="11%">
										Action
									</th>
								</tr>
							</thead>
							<tbody>
								{foreach $data item=$item key=$key}
								<tr>
									<td>
										{$key+1}.
									</td>
									<td>
										{$item.kodebarang}
									</td>
									<td>
										{$item.namabarang}
									</td>
									<td>
										<a class="btn btn-danger" href="{$root}/barang/hapus/{$item.kodebarang}" id="hapus">
										<i class="material-icons">delete
											</i>
										</a>
										<a class="btn btn-warning" href="{$root}/barang/edit/{$item.kodebarang}">
											<i class="material-icons">mode_edit
											</i>
										</a>
									</td>
								</tr>
								{/foreach}
							</tbody>
							<tfoot>
								<tr>
									<td colspan="4">
										Total : {count($data)} {$title}
									</td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
{/block}