{extends file=$app_tpl}
{block name="content"}

<section class="content">
	<div class="container-fluid">
		<div class="row clearfix">
			<div class="card">
				<div class="header">
					<h2>
						{$title} <small>Tambah Data {$title}</small>
					</h2>
					<ul class="header-dropdown m-r-0">
						<li>
							<button class="btn btn-primary" onclick="$('#form-tambah-barang').submit()"> <i class="material-icons" style="color: white">save</i> Simpan </button>
						</li>
						<li>
							<a class="btn btn-default" href="{$root}/barang"> <i class="material-icons" style="color: black">view_list</i> Lihat Data </a>
						</li>
					</ul>
				</div>
				<div class="body">
					
					<form id="form-tambah-barang">
						<div class="row clearfix">
							<div class="col-md-12">

								<div class="form-group form-float">

									<div class="form-line">

										<input class="form-control" name="kodebarang"></input>
										<label class="form-label">Kode Barang</label>	
									</div>
								</div>
								<div class="form-group form-float">
									<div class="form-line">
										<input class="form-control" name="namabarang"></input>
										<label class="form-label">Kode Barang</label>	
									</div>
								</div>
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	$('#form-tambah-barang').submit(function(e){
		e.preventDefault();
		if(!confirm('Apakah anda ingin menyimpan data ini?')){
			return false;
		}
		var effect = 'ios';
		var $loading = $('#form-tambah-barang').parents('.card').waitMe({
			effect: effect,
			text: 'Loading...',
			bg: 'rgba(255,255,255,0.90)',
			color: '#555'
		});
		$.ajax({
			url : '{$root}/barang/simpan',
			type : 'post',
			data : $('#form-tambah-barang').serialize(),
			complete : function(){
				$loading.waitMe('hide');
			},
			success : function(response){
				if(response.success){
					toastr['success'](response.message);
					$('#form-tambah-barang')[0].reset();
				}
				else{
					toastr['error'](response.message);	
				}
			},
			error : function(){
				toastr['error']('Data gagal disimpan');
			}
		});
	});
</script>
{/block}