<title>
	Laporan
</title>
<table width="70%" align="center">
	<tr>
		<td width="10%">
			
		</td>
		<td align="center">
			<h3>
				SISTEM PENDUKUNG KEPUTUSAN KELAYAKAN KREDITUR SEPEDA MOTOR MENGGUNAKAN METODE FUZZY TSUKAMOTO
			</h3>
		</td>
		<td width="10%">
			
		</td>
	</tr>
</table>
<table border="1" width="70%" align="center">
	<thead>
		<tr>
			<th width="2%">
				No.
			</th>
			<th>
				Kode Customer
			</th>
			<th>
				Nama Customer
			</th>
			<th>
				Keterangan
			</th>
		</tr>
	</thead>
	<tbody>
		{foreach from=$hasil item=item key=key name=name}
		<tr>
			<td>
				{$key+1}.
			</td>
			<td>
				{$item.kodecustomer}
			</td>
			<td>
				{$item.namacustomer}
			</td>
			<td>
				{$item.keterangan}
			</td>
		</tr>
		{/foreach}
	</tbody>
</table>
<br>
<br>

<table width="15%" align="center">
	<tr>
		<td>
			Medan, .................. <br>
			Diketahui oleh <br>
			<br>
			<br>
			<br>

			(.......................) 
		</td>
	</tr>
</table>

	<script type="text/javascript">
		window.print();
	</script>