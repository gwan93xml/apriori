{extends file=$app_tpl}
{block name="content"}
<div class="box box-primary">
	<div class="box-header with-border">
		<div class="box-title">
			Data {$title}

		</div>
<div class="box-tools pull-right">
			<a href="{$root}/laporan/print" class="btn btn-success" target="_blank"><i class="fa fa-print"></i> Print Data</a>
		</div>
		<div class="box-body">
			<table class="table table-bordered table-condensed">
				<thead>
					<tr>
						<th width="2%">
							No.
						</th>
						<th>
							Kode Customer
						</th>
						<th>
							Nama Customer
						</th>
						<th>
							Keterangan
						</th>
					</tr>
				</thead>
				<tbody>
					{foreach from=$hasil item=item key=key name=name}
						<tr>
							<td>
								{$key+1}.
							</td>
							<td>
								{$item.kodecustomer}
							</td>
							<td>
								{$item.namacustomer}
							</td>
							<td>
								{$item.keterangan}
							</td>
							<td>
								
							</td>
						</tr>
					{/foreach}
				</tbody>
			</table>
		</div>
	</div>
	{/block}