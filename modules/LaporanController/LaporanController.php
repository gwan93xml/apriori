<?php 
class LaporanController
{
    protected $smarty;
    protected $db;
    protected $vPath = 'modules/LaporanController/Views/';
function __construct($container) {
     $this->smarty = $container->smarty;
     $this->db = $container->db;
     $this->smarty->assign('active', 7);
 }
 function index($request, $response, $args) {
    $hasil = $this->db->getAll('SELECT * FROM hasil');
    $this->smarty->assign('hasil',$hasil);
    $this->smarty->assign('title','Laporan');
    return $this->smarty->display($this->vPath.'view.tpl');
}
function cetak($request, $response, $args) {
    $hasil = $this->db->getAll('SELECT * FROM hasil');
    $this->smarty->assign('hasil',$hasil);
    $this->smarty->assign('title','Laporan');
    return $this->smarty->display($this->vPath.'print.tpl');
}
}