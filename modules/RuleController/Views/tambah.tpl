{extends file=$app_tpl}
{block name="content"}
<div class="box box-primary">
	<div class="box-header with-border">
		<div class="box-title">
			Tambah Data {$title}
		</div>
	</div>
	<div class="box-body">
		<form id="form-tambah-customer">
			<div class="col-md-6">
				<div class="form-group">
					<label>
						Kode Customer :
					</label>
					<input class="form-control" name="kodecustomer"></input>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>
						Nama Customer :
					</label>
					<input class="form-control" name="namacustomer"></input>
				</div>
			</div>
		</form>
	</div>

	<div class="box-footer with-border">
		<div class="box-tools pull-right">
			<div class="col-md-12">
				<button class="btn btn-primary" onclick="$('#form-tambah-customer').submit()"> <i class="fa fa-save"></i> Simpan </button>
				<a class="btn btn-default" href="{$root}/customer"> <i class="fa fa-align-justify"></i> Lihat Data </a>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#form-tambah-customer').submit(function(e){
		e.preventDefault();
		if(!confirm('Apakah anda ingin menyimpan data ini?')){
			return false;
		}
		$.ajax({
			url : '{$root}/customer/simpan',
			type : 'post',
			data : $('#form-tambah-customer').serialize(),
			success : function(response){
				if(response.success){
					toastr['success'](response.message);
					$('#form-tambah-customer')[0].reset();
				}
				else{
					toastr['error'](response.message);	
				}
			},
			error : function(){
				toastr['error']('Data gagal disimpan');
			}
		});
	});
</script>
{/block}