{extends file=$app_tpl}
{block name="content"}
<div class="box box-primary">
	<div class="box-header with-border">
		<div class="box-title">
			Data {$title}
		</div>
		<div class="box-tools pull-right">
			<button style="position: fixed; left :19%" class="btn btn-primary" onClick="$('#frm-rules').submit();"><i class="fa fa-save"></i> Simpan Rules</a>
		</div>
	</div>
	<div class="box-body">
		<form id="frm-rules">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>
							Rules 
						</th>
						{foreach $kriteria item=$item key=$key}
						<th>
							{$item.namakriteria}
						</th>
						{/foreach}
						<th>
							Hasil
						</th>
					</tr>
				</thead>
				<tbody>
					{for $x=0 to $totalrule}
					<tr>
						<td>
							Rule {$x+1}
						</td>
						{foreach $kriteria item=$item key=$key}
						<td>
							<select name="R[{$x+1}][{$item['kodekriteria']}]" class="form-control">
								{foreach from=$item.subkriteria item=item1 key=key1}
								{if $item.useselect == 'Y'}
								<option value="{$item1.subkriteria}" {if isset($dtrule[$x]['subrule'][$key]['subkriteria'])}{if $dtrule[$x]['subrule'][$key]['subkriteria'] == $item1.subkriteria} selected {/if}{/if}>{$item1.label}</option>
								{else}
								<option value="{$item1.subkriteria}" {if isset($dtrule[$x]['subrule'][$key]['subkriteria'])}{if $dtrule[$x]['subrule'][$key]['subkriteria'] == $item1.subkriteria} selected {/if}{/if}>{$item1.subkriteria}</option>
								{/if}
								{/foreach}
							</select>
						</td>
						{/foreach}
						<td>
							<select name="Hasil[{$x+1}]" class="form-control">
								{foreach from=$hasil.subkriteria item=item1 key=key1}
								<option value="{$item1.subkriteria}" {if isset($dtrule[$x]['hasil'])} {if $dtrule[$x]['hasil'] == $item1.subkriteria } selected {/if}{/if}>{$item1.subkriteria}</option>
								{/foreach}
							</select>
						</td>
					</tr>
					{/for}
				</tbody>
			</table>
		</form>
	</div>
</div>
<script type="text/javascript">
	$('#frm-rules').submit(function(e){
		e.preventDefault();
		if(!confirm('Apakah anda ingin menyimpan data ini?')){
			return false;
		}
		$.ajax({
			url : '{$root}/rule/simpan',
			type : 'post',
			data : $('#frm-rules').serialize(),
			success : function(response){
				if(response.success){
					toastr['success'](response.message);
					$('#form-tambah-kriteria')[0].reset();
				}
				else{
					toastr['error'](response.message);	
				}
			},
			error : function(){
				toastr['error']('Data gagal disimpan');
			}
		});
	});
</script>
{/block}