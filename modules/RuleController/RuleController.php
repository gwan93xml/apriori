<?php 
class RuleController
{
    protected $smarty;
    protected $db;
    protected $vPath = 'modules/RuleController/Views/';
    function __construct($container) {
      	$this->smarty = $container->smarty;
      	$this->db = $container->db;
        $this->smarty->assign('active', 4);
    }
    function index($request, $response, $args) {
        $kriteria = $this->db->getAll('SELECT * FROM kriteria where hasil="N"');
        $hasil = $this->db->getRow('SELECT * FROM kriteria where hasil="Y"');
        $totalrule = 1;
        foreach ($kriteria as $key => $value) {
            $kriteria[$key]['subkriteria'] =  $this->db->getAll('SELECT * FROM subkriteria where kodekriteria=?',[$value['kodekriteria']]);
            $totalrule *=count($kriteria[$key]['subkriteria']);
        }
        $hasil['subkriteria'] = $this->db->getAll('SELECT * FROM subkriteria where kodekriteria=?',[$hasil['kodekriteria']]);
        $dtrule = $this->db->getAll(' SELECT * FROM rule order by id');
        foreach ($dtrule as $key => $value) {
            $dtrule[$key]['subrule'] = $this->db->getAll(' SELECT * FROM subrule where koderule=?',[$value['koderule']]);
        }
        // echo '<pre>';
        // print_r($dtrule);
        // echo '</pre>';
        // die();
        $this->smarty->assign('totalrule',$totalrule-1);
        $this->smarty->assign('dtrule',$dtrule);
        $this->smarty->assign('hasil',$hasil);
        $this->smarty->assign('kriteria',$kriteria);
        $this->smarty->assign('title','Rule');
        return $this->smarty->display($this->vPath.'view.tpl');
    }
    function simpan($request, $response, $args) {
        $rules = $request->getParams()['R'];
        $hasil = $request->getParams()['Hasil'];
        $this->db->exec(' DELETE FROM rule ');
        $this->db->exec(' DELETE FROM subrule ');
        foreach ($rules as $key => $value) {
            $dtrules[$key] = $this->db->dispense('rule');
            $dtrules[$key]->koderule = 'R'.$key;
            foreach ($value as $key1 => $value1) {
                $dtsubrules[$key." ".$key1] = $this->db->dispense('subrule');
                $dtsubrules[$key." ".$key1]->koderule = "R".$key;
                $dtsubrules[$key." ".$key1]->kriteria = $key1;
                $dtsubrules[$key." ".$key1]->subkriteria = $value1;
            }
            $dtrules[$key]->hasil = $hasil[$key];
        }
        $this->db->storeAll($dtsubrules);
        $this->db->storeAll($dtrules);
        $res['success'] = true;
        $res['message'] = 'Data rules berhasil disimpan' ;        
        return $response->withJson($res);
    }
    function update($request, $response, $args) {
        $kodecustomer = $request->getParams()['kodecustomer'];
        $namacustomer = $request->getParams()['namacustomer'];
        $customer = $this->db->findOne('customer','kodecustomer = ?', [$kodecustomer]);
        if(!count($customer)){
            $res['success'] = false;
            $res['message'] = 'Maaf data dengan kode customer ini tidak ada';
            return $response->withJson($res);
        }
        $customer->namacustomer = $namacustomer;
        $this->db->store($customer);
        $res['success'] = true;
        $res['message'] = 'Data customer berhasil diupdate' ;        
        return $response->withJson($res);
    }
    function hapus($request, $response, $args) {
        $customer = $this->db->findOne('customer','kodecustomer = ?', [$args['id']]);
        $this->db->trash($customer);
        $res['success'] = true;
        $res['message'] = 'Data customer berhasil dihapus' ;        
        return $response->withJson($res);
    }
}