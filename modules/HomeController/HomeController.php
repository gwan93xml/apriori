<?php 
class HomeController
{
    protected $smarty;
    protected $db;
    protected $vPath = 'modules/HomeController/Views/';
    function __construct($container) {
      	$this->smarty = $container->smarty;
      	$this->db = $container->db;
        $this->smarty->assign('active', 1);
    }
    function index($request, $response, $args) {
    	$kecamatan = $this->db->getAll('SELECT * FROM kecamatan');
    	$this->smarty->assign('kecamatan',$kecamatan);
    	$this->smarty->assign('title','Dashboard');
    	return $this->smarty->display($this->vPath.'view.tpl');
    }
}