{extends file=$app_tpl}
{block name="content"}
<div class="box box-primary">
	<div class="box-header with-border">
		<div class="box-title">
			Edit Data {$title}
		</div>
	</div>
	<div class="box-body">

		<form id="form-edit-penilaian">
			<div class="col-md-6">
				<div class="form-group">
					<label>
						Customer :
					</label>
					<input type="hidden" value="{$data[0].kodecustomer}" name="kodecustomer">
					<input class="form-control" value="{$data[0].namacustomer}" readonly=""></input>
				</div>
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>
								Kriteria
							</th>
							<th>
								Nilai
							</th>
						</tr>
					</thead>
					<tbody>
						{foreach $kriteria item=item key=key}
						<tr>
							<td>
								{$item.namakriteria}
							</td>
							<td>
								{if $item.useselect == 'Y'}
								<select class="form-control" name="nilai[{$item.kodekriteria}]">
									{$nilai = 3}
									{foreach from=$item['subkriteria'] item=item1 key=key1}
									<option value="{$nilai}" {if $nilai == $data[$key]['nilai']} selected="" {/if}>{$item1.label}</option>
									{$nilai = $nilai + 2}
									{/foreach}
								</select>
								{else}
								<input class="form-control" name="nilai[{$item.kodekriteria}]" value="{$data[$key]['nilai']}"></input>
								{/if}
							</td>
						</tr>
						{/foreach}
					</tbody>
				</table>
			</div>
		</form>
	</div>

	<div class="box-footer with-border">
		<div class="box-tools pull-right">
			<div class="col-md-12">
				<button class="btn btn-warning" onclick="$('#form-edit-penilaian').submit()"> <i class="fa fa-pencil"></i> Update </button>
				<a class="btn btn-default" href="{$root}/penilaian"> <i class="fa fa-align-justify"></i> Lihat Data </a>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#form-edit-penilaian').submit(function(e){
		e.preventDefault();
		if(!confirm('Apakah anda ingin menyimpan data ini?')){
			return false;
		}
		$.ajax({
			url : '{$root}/penilaian/update',
			type : 'post',
			data : $('#form-edit-penilaian').serialize(),
			success : function(response){
				if(response.success){
					toastr['success'](response.message);
					window.location.replace('{$root}/penilaian');
				}
				else{
					toastr['error'](response.message);	
				}
			},
			error : function(){
				toastr['error']('Data gagal diupdate');
			}
		});
	});
</script>
{/block}