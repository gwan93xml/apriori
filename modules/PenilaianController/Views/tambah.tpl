{extends file=$app_tpl}
{block name="content"}
<div class="box box-primary">
	<div class="box-header with-border">
		<div class="box-title">
			Tambah Data {$title}
		</div>
	</div>
	<div class="box-body">
		<form id="form-tambah-penilaian">
			<div class="col-md-6">
				<div class="form-group">
					<label>
						Customer :
					</label>
					<select class="form-control" name="customer">
						{foreach from=$customer item=item key=key name=name}
						<option value="{$item.kodecustomer}">{$item.namacustomer}</option>		
						{/foreach}	
					</select>
				</div>
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>
								Kriteria
							</th>
							<th>
								Nilai
							</th>
						</tr>
					</thead>
					<tbody>
						{foreach $kriteria item=item key=key}
						<tr>
							<td>
								{$item.namakriteria}
							</td>
							<td>
								{if $item.useselect == 'Y'}
								<select class="form-control" name="nilai[{$item.kodekriteria}]">
									{$nilai = 3}
									{foreach from=$item['subkriteria'] item=item1 key=key1}
									<option value="{$nilai}">{$item1.label}</option>
									{$nilai = $nilai + 2}
									{/foreach}
								</select>
								{else}
								<input class="form-control" name="nilai[{$item.kodekriteria}]"></input>
								{/if}
							</td>
						</tr>
						{/foreach}
					</tbody>
				</table>
			</div>
		</form>
	</div>

	<div class="box-footer with-border">
		<div class="box-tools pull-right">
			<div class="col-md-12">
				<button class="btn btn-primary" onclick="$('#form-tambah-penilaian').submit()"> <i class="fa fa-save"></i> Simpan </button>
				<a class="btn btn-default" href="{$root}/penilaian"> <i class="fa fa-align-justify"></i> Lihat Data </a>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#form-tambah-penilaian').submit(function(e){
		e.preventDefault();
		if(!confirm('Apakah anda ingin menyimpan data ini?')){
			return false;
		}
		$.ajax({
			url : '{$root}/penilaian/simpan',
			type : 'post',
			data : $('#form-tambah-penilaian').serialize(),
			success : function(response){
				if(response.success){
					toastr['success'](response.message);
					$('#form-tambah-penilaian')[0].reset();
				}
				else{
					toastr['error'](response.message);	
				}
			},
			error : function(){
				toastr['error']('Data gagal disimpan');
			}
		});
	});
</script>
{/block}