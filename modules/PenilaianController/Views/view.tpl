{extends file=$app_tpl}
{block name="content"}
<div class="box box-primary">
	<div class="box-header with-border">
		<div class="box-title">
			Data {$title}
		</div>
		<div class="box-tools pull-right">
			<a href="{$root}/penilaian/tambah" class="btn btn-primary" ><i class="fa fa-plus"></i> Tambah Penilaian</a>
		</div>
	</div>
	<div class="box-body">
		<form id="frm-rules">
			<table class="table table-bordered">
				<thead>
					<tr>
					<th width="5%">
						No.
					</th>
						<th width="20%">
							Kode Customer 
						</th>
						<th>
							Nama Customer
						</th>	
						<th width="10%">
							Action
						</th>
					</tr>
				</thead>
				<tbody>
					{foreach $data item=$item key=$key}
					<tr>
						<td>
							{$key+1}.
						</td>
						<td>
							{$item.kodecustomer}
						</td>
						<td>
							{$item.namacustomer}
						</td>
						<td>

							<a class="btn btn-danger" href="{$root}/penilaian/hapus/{$item.kodecustomer}" id="hapus">
								<i class="fa fa-trash">
								</i>
							</a>
							<a class="btn btn-warning" href="{$root}/penilaian/edit/{$item.kodecustomer}">
								<i class="fa fa-pencil">
								</i>
							</a>
						</td>
					</tr>
					{/foreach}
				</tbody>
			</table>
		</form>
	</div>
</div>
{/block}