<?php 
class PenilaianController
{
    protected $smarty;
    protected $db;
    protected $vPath = 'modules/PenilaianController/Views/';
    function __construct($container) {
      	$this->smarty = $container->smarty;
      	$this->db = $container->db;
        $this->smarty->assign('active', 5);
    }
    function index($request, $response, $args) {
        $customer = $this->db->getAll(' SELECT customer.kodecustomer, namacustomer FROM penilaian INNER JOIN customer on customer.kodecustomer = penilaian.kodecustomer Group By customer.kodecustomer ');
        $this->smarty->assign('data',$customer);
        $this->smarty->assign('title','Penilaian');
        return $this->smarty->display($this->vPath.'view.tpl');
    }
    function tambah($request, $response, $args) {
        $customer = $this->db->getAll(' SELECT * FROm customer order by kodecustomer');
        $this->smarty->assign('customer',$customer);

        $kriteria = $this->db->getAll(' SELECT * FROm kriteria where hasil="N" order by kodekriteria');
        foreach ($kriteria as $key => $value) {
            $kriteria[$key]['subkriteria'] = $this->db->getAll(' SELECT * FROM subkriteria where kodekriteria=?',[$value['kodekriteria']]); 
        }
        $this->smarty->assign('kriteria',$kriteria);
        $this->smarty->assign('title','Penilaian');
        return $this->smarty->display($this->vPath.'tambah.tpl');
    }
    function simpan($request, $response, $args) {
        $nilai = $request->getParams()['nilai'];
        $kodecustomer = $request->getParams()['customer'];
        $cekcustomer = $this->db->getRow(' SELECT * FROM penilaian where kodecustomer=?', [$kodecustomer]);
        if(count($cekcustomer)){
            $res['success'] = false;
            $res['message'] = 'Maaf data dengan kode customer ini sudah ada';
            return $response->withJson($res);
        }
        foreach ($nilai as $key => $value) {
            $penilaian[$key] = $this->db->dispense('penilaian');
            $penilaian[$key]->kodecustomer = $kodecustomer;
            $penilaian[$key]->kodekriteria = $key;
            $penilaian[$key]->nilai = $value;
        }
        $this->db->storeAll($penilaian);
        $res['success'] = true;
        $res['message'] = 'Data penilaian berhasil disimpan' ;        
        return $response->withJson($res);
    }

    function edit($request, $response, $args) {
        $data = $this->db->getAll(' SELECT * FROM penilaian INNER JOIN customer on customer.kodecustomer = penilaian.kodecustomer WHERE customer.kodecustomer=? order by penilaian.id',[$args['id']]);
        $customer = $this->db->getAll(' SELECT * FROm customer order by kodecustomer');
        $this->smarty->assign('data',$data);
        $this->smarty->assign('customer',$customer);
        $kriteria = $this->db->getAll(' SELECT * FROm kriteria where hasil="N" order by kodekriteria');
        foreach ($kriteria as $key => $value) {
            $kriteria[$key]['subkriteria'] = $this->db->getAll(' SELECT * FROM subkriteria where kodekriteria=?',[$value['kodekriteria']]); 
        }
        $this->smarty->assign('kriteria',$kriteria);
        $this->smarty->assign('title','Penilaian');
        return $this->smarty->display($this->vPath.'edit.tpl');
    }
    function update($request, $response, $args) {
        $nilai = $request->getParams()['nilai'];
        $kodecustomer = $request->getParams()['kodecustomer'];
        $cekcustomer = $this->db->getRow(' SELECT * FROM penilaian where kodecustomer=?', [$kodecustomer]);
        if(!count($cekcustomer)){
            $res['success'] = false;
            $res['message'] = 'Maaf data dengan kode customer ini tidak ada';
            return $response->withJson($res);
        }
        $this->db->exec(' DELETE FROM penilaian where kodecustomer="'.$kodecustomer.'"');
        foreach ($nilai as $key => $value) {
            $penilaian[$key] = $this->db->dispense('penilaian');
            $penilaian[$key]->kodecustomer = $kodecustomer;
            $penilaian[$key]->kodekriteria = $key;
            $penilaian[$key]->nilai = $value;
        }
        $this->db->storeAll($penilaian);
        $res['success'] = true;
        $res['message'] = 'Data penilaian berhasil diupdate' ;        
        return $response->withJson($res);
    }
    function hapus($request, $response, $args) {
        $this->db->exec(' DELETE FROM penilaian where kodecustomer="'.$args['id'].'"');
        $res['success'] = true;
        $res['message'] = 'Data penilaian berhasil dihapus' ;        
        return $response->withJson($res);
    }
}