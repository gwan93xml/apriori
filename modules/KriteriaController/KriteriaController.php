<?php 
class kriteriaController
{
    protected $smarty;
    protected $db;
    protected $vPath = 'modules/KriteriaController/Views/';
    protected $vsubkriteria = 'modules/KriteriaController/Views/itemsubkriteria.tpl';
    function __construct($container) {
      	$this->smarty = $container->smarty;
      	$this->db = $container->db;
        $this->smarty->assign('vsubkriteria', $this->vsubkriteria);
        $this->smarty->assign('active', 3);
    }
    function index($request, $response, $args) {
        $data = $this->db->getAll('SELECT * FROM kriteria order by kodekriteria');
        $this->smarty->assign('data',$data);
        $this->smarty->assign('title','Kriteria');
        return $this->smarty->display($this->vPath.'view.tpl');
    }
    function tambah($request, $response, $args) {
        $this->smarty->assign('title','Kriteria');
        return $this->smarty->display($this->vPath.'tambah.tpl');
    }
    function edit($request, $response, $args) {
        $data = $this->db->findOne('kriteria','kodekriteria = ?', [$args['id']]);
        $subkriteria = $this->db->getAll('SELECT * FROM subkriteria where kodekriteria=? order by id',[$args['id']]);
        $this->smarty->assign('data',$data);
        $this->smarty->assign('subkriteria',$subkriteria);
        $this->smarty->assign('title','Kriteria');
        return $this->smarty->display($this->vPath.'edit.tpl');
    }
    function simpan($request, $response, $args) {
        $kodekriteria = $request->getParams()['kodekriteria'];
        $namakriteria = $request->getParams()['namakriteria'];
        $useselect = $request->getParams()['useselect'];
        $hasil = $request->getParams()['hasil'];
        $subkriteria = $request->getParams()['subkriteria'];
        $dari = $request->getParams()['dari'];
        $hingga = $request->getParams()['hingga'];
        $label = $request->getParams()['label'];
        $cek = $this->db->findOne('kriteria','kodekriteria = ?', [$kodekriteria]);
        if(count($cek)){
            $res['success'] = false;
            $res['message'] = 'Maaf data dengan kode kriteria ini sudah ada';
            return $response->withJson($res);
        }
        $kriteria = $this->db->dispense('kriteria');
        $kriteria->kodekriteria = $kodekriteria;
        $kriteria->namakriteria = $namakriteria;
        $kriteria->useselect = $useselect;
        $kriteria->hasil = $hasil;
        foreach ($subkriteria as $key => $value) {
            $dtsubkriteria[$key] = $this->db->dispense('subkriteria');
            $dtsubkriteria[$key]->kodekriteria = $kodekriteria;
            $dtsubkriteria[$key]->subkriteria = $subkriteria[$key];
            $dtsubkriteria[$key]->dari = $dari[$key];
            $dtsubkriteria[$key]->hingga = $hingga[$key];
            $dtsubkriteria[$key]->label = $label[$key];
        }
        $this->db->storeAll($dtsubkriteria);
        $this->db->store($kriteria);
        $res['success'] = true;
        $res['message'] = 'Data kriteria berhasil disimpan' ;        
        return $response->withJson($res);
    }
    function update($request, $response, $args) {
        $kodekriteria = $request->getParams()['kodekriteria'];
        $namakriteria = $request->getParams()['namakriteria'];
        $useselect = $request->getParams()['useselect'];
        $hasil = $request->getParams()['hasil'];

        $subkriteria = $request->getParams()['subkriteria'];
        $dari = $request->getParams()['dari'];
        $hingga = $request->getParams()['hingga'];
        $label = $request->getParams()['label'];

        $kriteria = $this->db->findOne('kriteria','kodekriteria = ?', [$kodekriteria]);
        if(!count($kriteria)){
            $res['success'] = false;
            $res['message'] = 'Maaf data dengan kode kriteria ini tidak ada';
            return $response->withJson($res);
        }
        $kriteria->namakriteria = $namakriteria;
        $kriteria->useselect = $useselect;
        $kriteria->hasil = $hasil;

        $dtsubkriteriaTrash = $this->db->find('subkriteria',' kodekriteria = ? ',[$kodekriteria]);
        $this->db->trashAll($dtsubkriteriaTrash);
        foreach ($subkriteria as $key => $value) {
            $dtsubkriteria[$key] = $this->db->dispense('subkriteria');
            $dtsubkriteria[$key]->kodekriteria = $kodekriteria;
            $dtsubkriteria[$key]->subkriteria = $subkriteria[$key];
            $dtsubkriteria[$key]->dari = $dari[$key];
            $dtsubkriteria[$key]->hingga = $hingga[$key];
            $dtsubkriteria[$key]->label = $label[$key];
        }
        $this->db->storeAll($dtsubkriteria);
        $this->db->store($kriteria);
        $res['success'] = true;
        $res['message'] = 'Data kriteria berhasil diupdate' ;        
        return $response->withJson($res);
    }
    function hapus($request, $response, $args) {
        $kriteria = $this->db->findOne('kriteria','kodekriteria = ?', [$args['id']]);
        $subkriteria = $this->db->find('subkriteria',' kodekriteria = ? ',[$args['id']]);
        $this->db->trashAll($subkriteria);
        $this->db->trash($kriteria);
        $res['success'] = true;
        $res['message'] = 'Data kriteria berhasil dihapus' ;        
        return $response->withJson($res);
    }
}