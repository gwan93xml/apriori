<tr>
	<td>
		<button class="btn btn-danger" type="button" id="btn-hapus-subkriteria">
			<i class="fa fa-trash"></i>
		</button>
	</td>
	<td>
		<input class="form-control" name="subkriteria[]" {if isset($subkriteria.subkriteria)} value="{$subkriteria.subkriteria}"{/if}></input>
	</td>
	<td>
		<div class="row">
			<div class="col-md-5">
				<input class="form-control" name="dari[]" {if isset($subkriteria.dari)}value="{$subkriteria.dari}"{/if}></input>
			</div>
			<div class="col-md-2">
				s/d
			</div>
			<div class="col-md-5">
				<input class="form-control" name="hingga[]" {if isset($subkriteria.hingga)}value="{$subkriteria.hingga}"{/if}></input>
			</div>
		</div>
	</td>
	<td>
		<input class="form-control" name="label[]" {if isset($subkriteria.label)}value="{$subkriteria.label}"{/if}></input>
	</td>
</tr>