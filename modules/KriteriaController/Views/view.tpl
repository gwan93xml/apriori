{extends file=$app_tpl}
{block name="content"}
<div class="box box-primary">
	<div class="box-header with-border">
		<div class="box-title">
			Data {$title}
		</div>
		<div class="box-tools pull-right">
			<a href="{$root}/kriteria/tambah" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>
		</div>
	</div>
	<div class="box-body">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th width="1%">
						No. 
					</th>
					<th width="15%">
						Kode Kriteria
					</th>
					<th>
						Nama Kriteria
					</th>
					<th width="10%">
						Action
					</th>
				</tr>
			</thead>
			<tbody>
				{foreach $data item=$item key=$key}
				<tr>
					<td>
						{$key+1}.
					</td>
					<td>
						{$item.kodekriteria}
					</td>
					<td>
						{$item.namakriteria}
					</td>
					<td>
						<a class="btn btn-danger" href="{$root}/kriteria/hapus/{$item.kodekriteria}" id="hapus">
							<i class="fa fa-trash">
							</i>
						</a>
						<a class="btn btn-warning" href="{$root}/kriteria/edit/{$item.kodekriteria}">
							<i class="fa fa-pencil">
							</i>
						</a>
					</td>
				</tr>
				{/foreach}
			</tbody>
			<tfoot>
				<tr>
					<td colspan="4">
						Total : {count($data)} Kriteria
					</td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>
{/block}