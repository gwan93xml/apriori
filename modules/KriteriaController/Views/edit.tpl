{extends file=$app_tpl}
{block name="content"}
<div class="box box-primary">
	<div class="box-header with-border">
		<div class="box-title">
			Edit Data {$title}
		</div>
	</div>
	<div class="box-body">
		<form id="form-edit-kriteria">
			<div class="col-md-6">
				<div class="form-group">
					<label>
						Kode Kriteria :
					</label>
					<input class="form-control" name="kodekriteria" value="{$data.kodekriteria}" readonly=""></input>
				</div>

				<div class="form-group">
					<label>
						Use Select :
					</label>
					<select class="form-control" name="useselect">
						<option value="Y" {if $data.useselect == 'Y'} selected="" {/if}>Y</option>
						<option value="N" {if $data.useselect == 'N'} selected="" {/if}>N</option>
					</select>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>
						Nama Kriteria :
					</label>
					<input class="form-control" name="namakriteria"  value="{$data.namakriteria}"></input>
				</div>

				<div class="form-group">
					<label>
						Hasil :
					</label>
					<select class="form-control" name="hasil">
						<option value="Y" {if $data.hasil == 'Y'} selected="" {/if}>Y</option>
						<option value="N" {if $data.hasil == 'N'} selected="" {/if}>N</option>
					</select>
				</div>
			</div>

			<div class="col-md-12">
			<table class="table table-bordered" id="table-subkriteria">
				<thead>
					<tr>
						<th colspan="3">
						<button class="btn btn-success" id="btn-tambah-subkriteria" type="button"><i class="fa fa-plus"></i> Tambah Sub Kriteria</button>
						</th>
					</tr>
					<tr>
						<th width="5%">
							
						</th>
						<th width="30%">
							Sub Kriteria
						</th>
						<th width="30%">
							Range
						</th>
						<th width="30%">
							Label
						</th>
					</tr>
					<tbody>
						{foreach from=$subkriteria item=item key=key name=name}
							{include file=$vsubkriteria  subkriteria=$item}
						{/foreach}
					</tbody>
				</thead>
			</table>
			</div>
		</form>
	</div>

	<div class="box-footer with-border">
		<div class="box-tools pull-right">
			<div class="col-md-12">
				<button class="btn btn-warning" onclick="$('#form-edit-kriteria').submit()"> <i class="fa fa-pencil"></i> Update </button>
				<a class="btn btn-default" href="{$root}/kriteria"> <i class="fa fa-align-justify"></i> Lihat Data </a>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">

	var subkriteriitem = `{include file=$vsubkriteria}`;
	$('#form-edit-kriteria').submit(function(e){
		e.preventDefault();
		if(!confirm('Apakah anda ingin menyimpan data ini?')){
			return false;
		}
		$.ajax({
			url : '{$root}/kriteria/update',
			type : 'post',
			data : $('#form-edit-kriteria').serialize(),
			success : function(response){
				if(response.success){
					toastr['success'](response.message);
					window.location.replace('{$root}/kriteria');
				}
				else{
					toastr['error'](response.message);	
				}
			},
			error : function(){
				toastr['error']('Data gagal diupdate');
			}
		});
	});

	$('#btn-tambah-subkriteria').click(function(e){
		$('#table-subkriteria tbody').append(subkriteriitem);
	});
	$('#table-subkriteria tbody').on('click','#btn-hapus-subkriteria', function(e){
		if($('#table-subkriteria tbody tr').length > 1){
			$(this).parents('tr').remove();
		} 
		else{
			toastr['error']('sub kriteria minimal 1 item');
		}
	});
</script>
{/block}