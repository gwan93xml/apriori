{extends file=$app_tpl}
{block name="content"}
<div class="box box-primary">
	<div class="box-header with-border">
		<div class="box-title">
			Tambah Data {$title}
		</div>
	</div>
	<div class="box-body">
		<form id="form-tambah-kriteria">
			<div class="col-md-6">
				<div class="form-group">
					<label>
						Kode Kriteria :
					</label>
					<input class="form-control" name="kodekriteria"></input>
				</div>
				<div class="form-group">
					<label>
						Use Select :
					</label>
					<select class="form-control" name="useselect">
						<option value="Y">Y</option>
						<option value="N">N</option>
					</select>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>
						Nama Kriteria :
					</label>
					<input class="form-control" name="namakriteria"></input>
				</div>

				<div class="form-group">
					<label>
						Hasil :
					</label>
					<select class="form-control" name="hasil">
						<option value="Y">Y</option>
						<option value="N">N</option>
					</select>
				</div>
			</div>
			<div class="col-md-12">
			<table class="table table-bordered" id="table-subkriteria">
				<thead>
					<tr>
						<th colspan="3">
						<button class="btn btn-success" id="btn-tambah-subkriteria" type="button"><i class="fa fa-plus"></i> Tambah Sub Kriteria</button>
						</th>
					</tr>
					<tr>
						<th width="5%">
							
						</th>
						<th width="30%">
							Sub Kriteria
						</th>
						<th width="30%">
							Range
						</th>
						<th width="30%">
							Label
						</th>
					</tr>
					<tbody>
						{include file=$vsubkriteria}
					</tbody>
				</thead>
			</table>
			</div>
		</form>
	</div>
	<div class="box-footer with-border">
		<div class="box-tools pull-right">
			<div class="col-md-12">
				<button class="btn btn-primary" onclick="$('#form-tambah-kriteria').submit()"> <i class="fa fa-save"></i> Simpan </button>
				<a class="btn btn-default" href="{$root}/kriteria"> <i class="fa fa-align-justify"></i> Lihat Data </a>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var subkriteriitem = `{include file=$vsubkriteria}`;
	$('#form-tambah-kriteria').submit(function(e){
		e.preventDefault();
		if(!confirm('Apakah anda ingin menyimpan data ini?')){
			return false;
		}
		$.ajax({
			url : '{$root}/kriteria/simpan',
			type : 'post',
			data : $('#form-tambah-kriteria').serialize(),
			success : function(response){
				if(response.success){
					toastr['success'](response.message);
					$('#form-tambah-kriteria')[0].reset();
				}
				else{
					toastr['error'](response.message);	
				}
			},
			error : function(){
				toastr['error']('Data gagal disimpan');
			}
		});
	});
	$('#btn-tambah-subkriteria').click(function(e){
		$('#table-subkriteria tbody').append(subkriteriitem);
	});
	$('#table-subkriteria tbody').on('click','#btn-hapus-subkriteria', function(e){
		if($('#table-subkriteria tbody tr').length > 1){
			$(this).parents('tr').remove();
		} 
		else{
			toastr['error']('sub kriteria minimal 1 item');
		}
	});
</script>
{/block}