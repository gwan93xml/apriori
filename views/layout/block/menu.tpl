<aside class="main-sidebar">
	<section class="sidebar">
		<div class="user-panel">
			<div class="pull-left image">
				<img src="{$root}/assets/img/administrator.png" class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p>{$nama}</p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<ul class="sidebar-menu" data-widget="tree">
			<li class="{if $active == 1} active {/if}">
				<a  href="{$root}">
					<i class="fa fa-home"></i>
					<span> Home</span>
				</a>
			</li>
			<li class="{if $active == 2} active {/if}">
				<a  href="{$root}/customer" >
					<i class="fa fa-users"></i>
					<span>Data Customer</span>
				</a>
			</li>
			<li class="{if $active == 3} active {/if}">
				<a  href="{$root}/kriteria" >
					<i class="fa fa-tag"></i>
					<span>Data Kriteria</span>
				</a>
			</li>
			<li class="{if $active == 4} active {/if}">
				<a  href="{$root}/rule" >
					<i class="fa fa-dashboard"></i>
					<span>Data Rule</span>
				</a>
			</li>
			<li class="{if $active == 5} active {/if}">
				<a  href="{$root}/penilaian" >
					<i class="fa fa-sort-numeric-desc"></i>
					<span>Penilaian</span>
				</a>
			</li>
			<li class="{if $active == 6} active {/if}">
				<a  href="{$root}/perhitungan" >
					<i class="fa fa-check"></i>
					<span>Perhitungan</span>
				</a>
			</li>
			<li class="{if $active == 7} active {/if}">
				<a  href="{$root}/laporan" >
					<i class="fa fa-file"></i>
					<span>Laporan</span>
				</a>
			</li>
		</ul>
	</section>
</aside>