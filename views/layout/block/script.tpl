<script src="{$root}/node_modules/jquery/dist/jquery.min.js"></script>
<script src="{$root}/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="{$root}/node_modules/jquery-ui/jquery-ui.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="{$root}/node_modules/toastr/build/toastr.min.js"></script>
<script src="{$root}/node_modules/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="{$root}/node_modules/fastclick/lib/fastclick.js"></script>
<script src="{$root}/assets/dist/js/adminlte.min.js"></script>
<script src="{$root}/node_modules/select2/dist/js/select2.full.min.js"></script>