<header class="main-header">
	<a href="./" class="logo">
		<span class="logo-mini"><b>FT</b></span>
		<span class="logo-lg">
		<b>
			Tsukamoto
		</b>
		</span>
	</a>
	<nav class="navbar navbar-static-top" role="navigation">
		<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img src="{$root}/assets/img/administrator.png" class="user-image"/>
						<span class="hidden-xs">{$nama}</span>
					</a>
					<ul class="dropdown-menu">
						<li class="user-footer">
							<div class="pull-right">
								<a href="javascript:logout()" class="btn btn-default btn-flat">Logout</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
</header>