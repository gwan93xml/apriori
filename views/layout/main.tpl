<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To | Bootstrap Based Admin Template - Material Design</title>
    <link rel="icon" href="{$root}/favicon.ico" type="image/x-icon">
	<link href="{$root}/node_modules/css-font-roboto/index.css" rel="stylesheet" type="text/css">
	<link href="{$root}/node_modules/material-icons-font/material-icons-font.css" rel="stylesheet" type="text/css">
	<link href="{$root}/node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
	<link href="{$root}/node_modules/node-waves/dist/waves.css" rel="stylesheet" />
	<link href="{$root}/node_modules/animate.css/animate.css" rel="stylesheet" />
	<link href="{$root}/node_modules/toastr/build/toastr.css" rel="stylesheet" />
	<link href="{$root}/assets/css/style.css" rel="stylesheet">
    <link href="{$root}/assets/css/themes/all-themes.css" rel="stylesheet" />
    <link href="{$root}/assets/waitme/waitMe.min.css" rel="stylesheet" />
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <div class="overlay"></div>
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">{$appname} - {$subappname}</a>
            </div>
        </div>
    </nav>
    <section>
        <aside id="leftsidebar" class="sidebar">
            <div class="user-info">
                <div class="image">
                    <img src="{$root}/assets/images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{$nama}</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li {if $active== 1} class="active" {/if}>
                        <a href="{$root}">
                            <i class="material-icons">home</i>
                            <span>Home</span>
                        </a>
                    </li>
                    <li {if $active == 2} class="active" {/if}>
                        <a href="{$root}/barang">
                            <i class="material-icons">widgets</i>
                            <span>Data Barang</span>
                        </a>
                    </li>
                    <li {if $active == 3} class="active" {/if}>
                        <a href="{$root}/penjualan">
                            <i class="material-icons">shopping_cart</i>
                            <span>Penjualan</span>
                        </a>
                    </li>
                    <li {if $active == 4} class="active" {/if}>
                        <a href="{$root}/prosesapriori">
                            <i class="material-icons">low_priority</i>
                            <span>Proses Apriori</span>
                        </a>
                    </li>
                    <li {if $active == 5} class="active" {/if}>
                        <a href="{$root}/penjualan">
                            <i class="material-icons">insert_drive_file</i>
                            <span>Laporan</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="legal">
                <div class="copyright">
                    &copy; 2017 <a href="javascript:void(0);">{$appname} {$subappname}</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.0
                </div>
            </div>
        </aside>
    </section>

	<script src="{$root}/node_modules/jquery/dist/jquery.min.js"></script>
	<script src="{$root}/node_modules/bootstrap/dist/js/bootstrap.js"></script>
    <script src="{$root}/node_modules/jquery-slimscroll/jquery.slimscroll.js"></script>
	<script src="{$root}/node_modules/node-waves/dist/waves.js"></script>
	<script src="{$root}/node_modules/toastr/build/toastr.min.js"></script>
	<script src="{$root}/node_modules/jquery-validation/dist/jquery.validate.js"></script>
    <script src="{$root}/assets/js/admin.js"></script>
    <script src="{$root}/assets/js/pages/index.js"></script>
    <script src="{$root}/assets/waitme/waitMe.min.js"></script>
    {block name="content"}
    {/block}
</body>

</html>